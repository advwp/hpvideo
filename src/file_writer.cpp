#include "file_writer.h"

#include <string>
#include "logger.h"

using std::string;

void FileWriter::WriteToAvi(const int serial_number, 
                            const int take_id, 
                            const float frame_rate, 
                            const std::vector<std::unique_ptr<FlyCapture2::Image>> &images) {

       FlyCapture2::AVIOption avi_option;
       avi_option.frameRate = frame_rate;

       string file_name;
       std::stringstream stream_out;
       stream_out << take_id << "." << serial_number << "." << frame_rate;
       file_name = stream_out.str();

       if (avi_recorder_.AVIOpen(file_name.c_str(), &avi_option) != FlyCapture2::PGRERROR_OK) {
                return;
       }

       for (auto &current_image : images) {
                avi_recorder_.AVIAppend(current_image.get());
       }

       avi_recorder_.AVIClose();
}

void FileWriter::WriteToJpeg(const int serial_number, 
                             const int take_id, 
                             const std::vector<std::unique_ptr<FlyCapture2::Image>> &images) {


        FlyCapture2::JPEGOption jpeg_option;
        jpeg_option.progressive = false;
        jpeg_option.quality = 50;

        int number_of_images = 0;
        for (auto &current_image : images) {
                string file_name;
                std::stringstream stream_out;
                stream_out << take_id << "." << serial_number << "." << number_of_images << ".jpeg";
                file_name = stream_out.str();
                current_image->Save(file_name.c_str(), &jpeg_option);
                number_of_images++;
        }
}
