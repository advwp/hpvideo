#include <QtGui>

#include "camera_adv_settings_page.h"

#include "controller.h"

CameraAdvSettingsPage::CameraAdvSettingsPage(std::shared_ptr<Controller> controller, 
                                             std::vector<unsigned int> &camera_vector, 
                                             QWidget *parent) : QWidget(parent) {
  setupUi(this);

  this->controller_ = controller;

  for (auto serial_number : camera_vector) {
    this->camera_vector_.push_back(serial_number);
  }

  Init();

}

void CameraAdvSettingsPage::Init() {
  std::unique_ptr<FlyCapture2::FC2Config> config;

  controller_->GetFC2Config(camera_vector_.at(0), config);

  spin_int_buf->setValue(config->numBuffers);
  spin_img_not->setValue(config->numImageNotifications);
  spin_grab_timeout->setValue(config->grabTimeout);

  for (auto &it : camera_vector_) {
    controller_->SetFC2Config(it, config);
  }
  
  ConnectSignals();
}

void CameraAdvSettingsPage::ConnectSignals() {

  connect(spin_int_buf, SIGNAL ( valueChanged(int) ), this, SLOT( InternalBuffersUpdated(int) ));
  connect(spin_img_not, SIGNAL ( valueChanged(int) ), this, SLOT( ImageNotificationsUpdated(int) ));
  connect(spin_grab_timeout, SIGNAL ( valueChanged(int) ), this, SLOT( GrabTimeoutUpdated(int) ));
}

void CameraAdvSettingsPage::InternalBuffersUpdated(int int_buf) {
  std::unique_ptr<FlyCapture2::FC2Config> config;

  controller_->GetFC2Config(camera_vector_.at(0), config);

  unsigned int num_buffers = config->numBuffers;

  config->numBuffers = static_cast<unsigned int>(int_buf);

  bool error = false;
  for (auto &it : camera_vector_) {
    if (!controller_->SetFC2Config(it, config)) {
      error = true;
    }
  }

  if (error) {
    spin_int_buf->setValue(static_cast<int>(num_buffers));
    config->numBuffers = num_buffers;

    for (auto &it : camera_vector_) {
      controller_->SetFC2Config(it, config);
    }
  }
  
}

void CameraAdvSettingsPage::ImageNotificationsUpdated(int img_not) {
  std::unique_ptr<FlyCapture2::FC2Config> config;

  controller_->GetFC2Config(camera_vector_.at(0), config);

  unsigned int num_img_not = config->numImageNotifications;

  config->numImageNotifications = static_cast<unsigned int>(img_not);

  bool error = false;
  for (auto &it : camera_vector_) {
    if (!controller_->SetFC2Config(it, config)) {
      error = true;
    }
  }

  if (error) {
    spin_img_not->setValue(static_cast<int>(num_img_not));
    config->numImageNotifications = num_img_not;

    for (auto &it : camera_vector_) {
      controller_->SetFC2Config(it, config);
    }
  }
}

void CameraAdvSettingsPage::GrabTimeoutUpdated(int g_time) {
  std::unique_ptr<FlyCapture2::FC2Config> config;

  controller_->GetFC2Config(camera_vector_.at(0), config);

  int grab_timeout = config->grabTimeout;

  config->grabTimeout = g_time;

  bool error = false;
  for (auto &it : camera_vector_) {
    if (!controller_->SetFC2Config(it, config)) {
      error = true;
    }
  }

  if (error) {
    spin_grab_timeout->setValue(grab_timeout);
    config->grabTimeout = grab_timeout;

    for (auto &it : camera_vector_) {
      controller_->SetFC2Config(it, config);
    }
  }
}
