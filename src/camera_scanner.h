#ifndef CAMERA_SCANNER_H
#define CAMERA_SCANNER_H

#include <vector>
#include <memory>

#include "FlyCapture2.h"

class CameraCon;
class CameraConLocal;
class CameraConRemote;

/*! \brief Scanner for connected cameras. 
* 
* This class is used by the Controller class to scan for 
* connected cameras. 
*/
class CameraScanner {

public:
  /*! \brief Scan local cameras. 
   * 
   * Function for scanning all local cameras. 
   * If the camera connection did not succeed, the camera is not
   * added to the vector. 
   * @param cameras vector containing pointers to all cameras found. 
   */
  void ScanLocalCameras(std::vector<std::unique_ptr<CameraConLocal>> &cameras);

  /*! \brief Scan local cameras. 
   * 
   * Function for scanning all remote cameras. 
   * If the camera connection did not succeed, the camera is not
   * added to the vector. 
   * @param cameras vector containing pointers to all cameras found. 
   */
  void ScanRemoteCameras(std::vector<std::unique_ptr<CameraConRemote>> &cameras);
 
  /* \brief Get all connected cameras. 
   * 
   * Uses the other functions in the class to make a list of all 
   * connected cameras. 
   * @param cameras vector containing pointers to all cameras found. 
   */
  void GetAllCameras(std::vector<std::unique_ptr<CameraCon>> &cameras);

};

#endif
