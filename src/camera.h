#ifndef CAMERA_H
#define CAMERA_H

#include "FlyCapture2.h"

#include <list>
#include <thread>
#include <memory>
#include <vector>
#include <mutex>

/*! \brief Class for controlling a single camera. 
*
* This class is used for controlling a single camera. 
* It uses its own image buffer for the recieved images. 
* Images are retrieved using the readBuffer or getNewImage functions. 
*/
class Camera {

public:

  /*! \brief Constructor for the Camera class. 
   *
   * The function takes a FlyCapture2::PGRGuid as input, and connects to it. 
   * If the connection is unsuccessful, the connected variable is set to false. 
   * @param guid PGRGuid for the camera. 
   */    
  Camera(FlyCapture2::PGRGuid guid);

  /*! \brief Destructor for the Camera class.
   *
   * The destructor attempts to stop capturing and disconnect the camera. 
   */
  ~Camera();

  /*! \brief Start the camera and capture thread.
   *
   * Attempts to start video capture from the camera and starts a capture thread which stores the recieved images in the image buffer. 
   * @param frame_rate frame rate of the capture. 
   * @return If the operation was successful or not. 
   */
  bool StartCapture();

  /*! \brief Stop the camera and capture thread. 
   *
   * Attempts to stop video capture from the camera, and joins the running capture thread. 
   * @return If the operation was successful or not. 
   */
  bool StopCapture();

  /*! \brief Start capturing from the camera. 
   *
   * Attempts to start video capture from the camera. 
   * This function does not start the capture thread, which means no images are stored on retrieval. 
   * stopCapture() is still used to stop the video capture. 
   * @return If the operation was successful or not. 
   */
  bool StartLiveView();

  /*! \brief Get an image from the image queue. 
   *
   * Store the oldest image in the image buffer in the provided pointer. 
   * After retrieval, the image is deleted from the buffer. 
   * @param image unique_ptr to the image object where the retrieved image will be stored. 
   * @return If the image retrieval was succesful. (False if the image buffer is empty)
   */
  bool ReadBuffer(std::unique_ptr<FlyCapture2::Image> &image); 

  /*! \brief Set a video mode and frame rate combination on the camera. 
   *
   * @param video_mode
   * @param frame_rate
   * @return if the operation was successful.
   */
  bool SetVideoModeAndFrameRate(const FlyCapture2::VideoMode video_mode, 
                                const FlyCapture2::FrameRate frame_rate);

  /*! \brief Set FC2Config on camera. 
   */
  bool SetFC2Config(const std::unique_ptr<FlyCapture2::FC2Config> &config);

  /*! \brief Get FC2Config on camera. 
   */
  bool GetFC2Config(std::unique_ptr<FlyCapture2::FC2Config> &config);

  /*! \brief Get a fresh image from the camera. 
   *
   * Gets a new image from the camera and stores it.
   * The camera has to be capturing images already.  
   * @param image Pointer to the image object which will contain the image. 
   * @return If the image retrieval was succesful.
   */
  bool GetNewImage(std::unique_ptr<FlyCapture2::Image> &image);

  /*! \brief Get information about the connected camera. 
   *
   * Reads information from the camera and stores it in the provided pointer. 
   * @param camInfo Pointer to the CameraInfo object where the information should be stored. 
   * @return If the operation was successful or not. 
   */
  bool ReadCameraInfo(std::unique_ptr<FlyCapture2::CameraInfo> &camera_info);

  /*! \brief Get the value(s) of a specific property. 
   *
   * Stores the value(s) on the provided property type in the provided pointer. 
   * The property is defined by the type argument. Available property types can be found in the FlyCapture2 documentation. 
   * @param property unique_ptr to the Property object where the information should be stored. 
   * @param type Which property the camera should return information on. 
   * @return If the operation was successful or not. 
   */
  bool ReadProperty(std::unique_ptr<FlyCapture2::Property> &property, 
                    FlyCapture2::PropertyType type);

  /*! \brief Get information on a specific property. 
   *      
   * Stores information connected to a specific property in the provided pointer. 
   * This can be allowed max/min values etc. 
   * @param property unique_ptr to the Property object where the information should be stored. 
   * @param type Which property the camera should return information on. 
   * @return If the operation was successful or not. 
   */
  bool ReadPropertyInfo(std::unique_ptr<FlyCapture2::PropertyInfo> &property_info, 
                        FlyCapture2::PropertyType type);

  /*! \brief Set a property. 
   *
   * Changes the value of a specific property on the camera. 
   * The provided property must have its type variable set to the desired property. 
   * @param property unique_ptr to the Property which should be set. 
   * @return If the operation was successful or not. 
   */
  bool SetProperty(const std::unique_ptr<FlyCapture2::Property> &property);

  /*!
   * Set the trigger status.
   * @return If the operation was successful or not. 
   */
  bool SetTrigger(const bool trigger_status);

  /*!
   * Get the trigger status. 
   * @return If the operation was successful or not. 
   */
  bool GetTrigger(bool &trigger);

  /*! \brief Fire software trigger. 
   *
   * Attempt to fire the software trigger. 
   * @return if it succeeded. 
   */
  bool FireSoftwareTrigger();

  /*!
   * Get the size of the buffer. 
  * @return Current size of the image buffer. 
  */
  int GetBufferSize() { return image_buffer_->size(); }

  /*!
  * Check if constructor succeded.
  * @return bool
  */
  bool IsConnected() { return connected_; }

  /*! \brief Check if a specific video mode is available on a specific frame rate. 
   *
   * @param videoMode The video mode to be checked. 
   * @param frameRate The frame rate to be checked. 
   * @return bool Whether or not the video mode + frame rate is supported. 
   */
  bool CheckVideoModeAndFrameRate(FlyCapture2::VideoMode video_mode, 
                                  FlyCapture2::FrameRate rame_rate);


private: 

  std::shared_ptr<std::list<std::unique_ptr<FlyCapture2::Image>>> image_buffer_; //!< Image buffer for the captured images.

  std::vector<std::thread> threads_; //!< Container for the capture thread. 

  /*! \brief Function launched by the capture thread. 
   *
   * This function is run by the capture thread, and is used to capture images from the camera.
   * The images captured from cam is stored in imageBuffer until captureInProgress == false. 
   * @param pcam Camera object. 
   * @param imageBuffer Buffer where the images should be stored. 
   * @param captureInProgress shared_ptr to the bool variable controlling when to stop the capture. 
   * @param image_buffer_mutex mutex for the image buffer
   * @param capturing_mutex mutex for controlling capture status
   */
  static void CaptureImages(const std::shared_ptr<FlyCapture2::Camera> cam, 
                            const std::shared_ptr<std::list<std::unique_ptr<FlyCapture2::Image>>> imageBuffer, 
                            const std::shared_ptr<bool> captureInProgress, 
                            const std::shared_ptr<std::mutex> capture_mutex, 
                            const std::shared_ptr<std::mutex> image_buffer_mutex);

  /*! \brief Check for dropped images. 
   *
   * This function compares timestamps from two images 
   * and calculates if there have been dropped images. 
   * @param fps frame rate
   * @param s1 seconds from first timestamp
   * @param mic_s1 microseconds from first timestamp
   * @param s1 seconds from second timestamp
   * @param mic_s2 microseconds from second timestamp
   * @return timestamps within range?
   */
  static bool ValidateTimeStamps(double fps, 
                                 long long s1, 
                                 unsigned int mic_s1, 
                                 long long s2, 
                                 unsigned int mic_s2);


  bool connected_; //!< The connection status of the camera. 

  std::shared_ptr<FlyCapture2::Camera> camera_; //!< Shared pointer to the camera object. 
  std::shared_ptr<bool> capture_in_progress_; //!< Shared pointer to the bool controlling the captures. 

  std::shared_ptr<std::mutex> image_buffer_mutex_; //!< Mutex for reading and writing to image buffer. 
  std::shared_ptr<std::mutex> capture_mutex_; //!< Mutex for controlling capture. 

  unsigned int serial_number_;

}; // Camera

const unsigned int kMaxBufferSize = 100; //!< Max size of the image buffer. 

#endif
