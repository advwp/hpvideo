#ifndef LIVE_VIEW_WINDOW_H
#define LIVE_VIEW_WINDOW_H

#include "ui_live_view_window.h"

#include <memory>

class Controller;

/*! \brief Class controlling live view window. 
*/
class LiveViewWindow : public QWidget, private Ui::win_live_view {

  Q_OBJECT

public: 

  /*! \brief Constructor
   * 
   * @param controller
   * @param camera_vector selected cameras. 
   * @param size size to set on the window. 
   * @param pos where it should be placed.
   * @param parent parent widget
   */
  LiveViewWindow(std::shared_ptr<Controller> controller, 
                 const unsigned int serial_number, 
                 const QSize size, 
                 const QPoint pos, 
                 QWidget *parent = 0);

  /*! \brief Destructor
   */
  ~LiveViewWindow();


public slots: 
  /*! \brief Update the image shown. 
   * 
   * This is called by the timer and changes the image to 
   * a new frame.
   */
  void UpdateImage();

private: 

  /*! \brief Initial setup
   */
  void Init();

  /*! \brief Connect signals
   */
  void ConnectSignals();

  /*! \brief Start the live view. 
   * 
   * Starts the timer which calls the update function. 
   */
  void Start();

  /*! \brief Stop the timer. 
   */
  void Stop();

  unsigned int serial_number_; //!< Camera used

  std::shared_ptr<Controller> controller_; //!< Controller

  QTimer *timer_; //!< Timer which calls the update function.

  bool started_; //!< True if the live view is started. 

};

#endif
