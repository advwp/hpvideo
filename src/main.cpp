#include <QApplication>

#include "main_window.h"

/*! \brief Application entry point. 
*/
int main(int argc, char *argv[]) {
        QApplication app(argc, argv);
        MainWindow *win_main = new MainWindow;

        win_main->show();
        return app.exec();
}
