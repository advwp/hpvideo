#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <map>
#include <memory>
#include <string>
#include <vector>
#include <thread>
#include <sstream>

#include "camera_scanner.h"

class CameraConLocal;

class Controller {

public:

  /*! \brief Constructor
   */
  Controller();

  /*! \brief Destructor
   */
  ~Controller();

  /*! \brief Start capture on cameras. 
   *
   * This function is used to start captuing on multiple cameras. 
   * The cameras will be put into trigger mode. If software trigger 
   * was selected, software trigger will be used. 
   * @param cameras
   * @param frame_rate
   * @return capture start successful
   */
  bool StartCapture(const std::vector<unsigned int> &cameras, 
                    const double frame_rate);

  /*! \brief Stop capture on all cameras.
   *
   * This function stops all captues. 
   */
  void StopCapture();

  /*! \brief Start live view from a camera. 
   *
   * The difference between this function and StartCapture() 
   * is that this function sets the camera into DROP_FRAME mode, 
   * and it does not save any incoming images. This also disables trigger mode.  
   * Only used for live capture.
   * @param serial_number
   * @return successfully started.
   */
  bool StartLiveView(const unsigned int serial_number);

  /*! \brief Stop live view from a camera. 
   *
   * This function stops capture on a single camera used in 
   * live viewing.
   * @param serial_number
   * @return capture stopped successfully
   */
  bool StopLiveView(const unsigned int serial_number);

  /*! \brief Update list of connected cameras. 
   *
   * This function updates the list of connected cameras. 
   * It also updates unfound cameras in the database. 
   */
  void UpdateCameras();

  /*! \brief Accessor function to map of cameras. 
   */
  std::map<unsigned int, std::shared_ptr<CameraConLocal>>* all_cameras() { return &all_cameras_; }

  /*! \brief Get number of cameras connected.
   */
  int GetNumberOfCameras() { return all_cameras_.size(); }
  
  int GetMaxNumberOfImagesRemaining() { return *max_images_remaining_; }
  int GetNumberOfImagesRemaining() { return *images_remaining_; }

  /*! \brief Change the name of a camera. 
   *
   * @param serial_number
   * @param name
   * @return successful?
   */
  bool ChangeCameraName(const unsigned int serial_number, const std::string name);

  /*! \brief Get a camera name. 
   *
   * @param serial_number
   * @param *OUT* name
   * @return successful?
   */
  bool GetCameraName(const unsigned int serial_number, std::string &name);

  /*! \brief Reads camera info from a camera. 
   *
   * @param serial_number 
   * @param camera_info
   * @return successful?
   */
  bool ReadCameraInfo(unsigned int serial_number, 
                      std::unique_ptr<FlyCapture2::CameraInfo> &camera_info);

  /*! \brief Reads property from a camera. 
   *
   * @param serial_number 
   * @param *OUT* property
   * @param property_type
   * @return successful?
   */
  bool ReadProperty(unsigned int serial_number, 
                    std::unique_ptr<FlyCapture2::Property> &property, 
                    FlyCapture2::PropertyType type);

  /*! \brief Sets a property on the camera.
   *  
   * @param serial_number 
   * @param  property
   * @return successful?
   */
  bool SetProperty(unsigned int serial_number, 
                   const std::unique_ptr<FlyCapture2::Property> &property);

  /*! \brief Change video mode and frame rate. 
   *
   * @param serial_number
   * @param video_mode, 
   * @param frame_rate
   */
  bool SetVideoModeAndFrameRate(const unsigned int serial_number, 
                                const FlyCapture2::VideoMode video_mode, 
                                const FlyCapture2::FrameRate frame_rate);

  /*! \brief Reads property info from a camera. 
   *
   * @param serial_number 
   * @param *OUT* property_info
   * @param property_type
   * @return successful?
   */
  bool ReadPropertyInfo(unsigned int serial_number, 
                        std::unique_ptr<FlyCapture2::PropertyInfo> &property_info, 
                        FlyCapture2::PropertyType type);

  /*! \brief sets FC2Config on a camera. 
   *
   * @param serial_number 
   * @param config
   * @return successful?
   */
  bool SetFC2Config(unsigned int serial_number, 
                    const std::unique_ptr<FlyCapture2::FC2Config> &config);

  /*! \brief gets FC2Config from a camera. 
   *
   * @param serial_number 
   * @param *OUT*config
   * @return successful?
   */
  bool GetFC2Config(unsigned int serial_number, 
                    std::unique_ptr<FlyCapture2::FC2Config> &config);

  /*! \brief gets trigger mode from a camera. 
   *
   * @param serial_number 
   * @param *OUT* trigger
   * @return successful?
   */
  bool GetTriggerOnOff(const unsigned int serial_number, 
                       bool &trigger);

  /*! \brief sets trigger mode on a camera. 
   *
   * @param serial_number 
   * @param trigger
   * @return successful?
   */
  bool SetTriggerOnOff(const unsigned int serial_number, 
                       const bool trigger);

  /*! \brief Fires the software trigger on the camera. 
   *
   * @param serial_number 
   * @return successful?
   */
  bool FireSoftwareTrigger(const unsigned int serial_number);

  /*! \brief Saves a camera to the database
   *
   * @param serial_number
   * @param name
   * @param model_name
   * @param vendor_name
   * @return successful?
   */
  bool SaveCamera(const unsigned int serial_number, 
                  const std::string name, 
                  const std::string model_name, 
                  const std::string vendor_name);

  /*! \brief Writes a take from a specific camera to AVI. 
   */
  void WriteImagesToAvi(const unsigned int serial_number, const int take_id, const float frame_rate);

  /*! \brief Get an image. 
   *
   * This is used to retrieve images during live view.
   * @param serial_number
   * @param *OUT* image_data
   * @param *OUT* image_data_size
   * @param *OUT* image_width
   * @param *OUT* image_height
   * @return successful?
   */
  bool GetImageData(const unsigned int serial_number, 
                    std::unique_ptr<unsigned char> &image_data, 
                    unsigned int &image_data_size, 
                    unsigned int &image_width, 
                    unsigned int &image_height);

  /*! \brief Check if a video mode and frame rate combination is available. 
   *
   * @param serial_number
   * @param video_mode
   * @param frame_rate
   */
  bool CheckVideoModeAndFrameRate(const unsigned int serial_number, 
                                  const FlyCapture2::VideoMode videoMode, 
                                  const FlyCapture2::FrameRate frameRate);

private:

  CameraScanner camera_scanner_; //!< Object used to scan for cameras. 

  std::map<unsigned int, std::shared_ptr<CameraConLocal>> all_cameras_; //!< Map of all cameras connected. 

  std::shared_ptr<bool> capturing_; //!< Variable controlling when to stop capturing.
  std::shared_ptr<std::mutex> capturing_mutex_; //!< Mutex for capturing_;
  std::shared_ptr<int> max_images_remaining_;
  std::shared_ptr<int> images_remaining_;

  bool record_;

  std::vector<std::thread> threads_; //!< Thread container

  /*! \brief Thread capture function. 
   *
   * This function runs in its own thread and captures images, 
   * then saves them in the database. Saving is stopped when capturing == false. 
   * @param cameras
   * @param capturing
   * @param capturing_mutex, 
   * @param frame_rate
   */
  static void RecordImages(const std::shared_ptr<std::vector<std::shared_ptr<CameraConLocal>>> cameras, 
                           const std::shared_ptr<bool> capturing,
                           const std::shared_ptr<std::mutex> capturing_mutex, 
                           const double frame_rate);

  /*! \brief Clear buffers to database. 
   *
   * This function is called by RecordImages after capture is complete. 
   * Any images remaining in the camera buffers are sent to the database. 
   * @param cameras
   * @param images_remaining
   * @param max_images_remaining
   */
  static void RecordBuffer(const std::shared_ptr<std::vector<std::shared_ptr<CameraConLocal>>> cameras, 
                           const std::shared_ptr<int> images_remaining, 
                           const std::shared_ptr<int> max_images_remaining);

}; // Controller

const std::string kDatabaseName = "database.db";

#endif // CONTROLLER_H
