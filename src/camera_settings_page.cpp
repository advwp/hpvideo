#include <QtGui>

#include "camera_settings_page.h"

#include "static_info.h"

#include "controller.h"


CameraSettingsPage::CameraSettingsPage(std::shared_ptr<Controller> controller, 
                                       std::vector<unsigned int> &camera_vector, 
                                       QWidget *parent, 
                                       MainWindow *main_window) : QWidget(parent) {
  setupUi(this);

  main_window_ = main_window;

  for (auto serial_number : camera_vector) {
    camera_vector_.push_back(serial_number);
  }

  this->controller_ = controller;

  Init();
}

CameraSettingsPage::~CameraSettingsPage() {
  timer_->stop();
}

void CameraSettingsPage::Init() {

  CreatePropertyMap();
  CreateWidgetMap();
  GetPropertyWidgets();

  slider_signal_mapper_ = new QSignalMapper(this);
  check_auto_signal_mapper_ = new QSignalMapper(this);
  check_on_off_signal_mapper_ = new QSignalMapper(this);
  spin_box_signal_mapper_ = new QSignalMapper(this);

  timer_ = new QTimer(this);

  if (camera_vector_.at(0) > 0) {
    ConnectSignals();
    timer_->start(500);
  }

  if (camera_vector_.size() > 1) {
    QString q_str = "Unavailable";
    line_edit_camera_name->setText(q_str);
    line_edit_camera_name->setReadOnly(true);
  } else {
    std::string name;
    if (controller_->GetCameraName(camera_vector_.at(0), name)) {
      QString q_str_name(name.c_str());
      line_edit_camera_name->setText(q_str_name);
    } else {
      line_edit_camera_name->setText("Name not found");
    }
  }

}

void CameraSettingsPage::ConnectSignals() {

  connect(btn_save, SIGNAL( clicked() ), this, SLOT( NameLineChanged() ));

  connect(slider_signal_mapper_, SIGNAL( mapped(int) ), this, SLOT( SliderChanged(int) ));
  connect(check_auto_signal_mapper_, SIGNAL( mapped(int) ), this, SLOT( AutoCheckBoxChanged(int) ));
  connect(check_on_off_signal_mapper_, SIGNAL( mapped(int) ), this, SLOT( OnOffCheckBoxChanged(int) ));
  connect(spin_box_signal_mapper_, SIGNAL( mapped(int) ), this, SLOT( SpinBoxChanged(int) ));
  

  for (unsigned int i = 0; i < kNumProps; i++) {

    FlyCapture2::PropertyType current_type = static_cast<FlyCapture2::PropertyType>(i);

    std::unique_ptr<FlyCapture2::Property> camera_property;
    std::unique_ptr<FlyCapture2::PropertyInfo> camera_property_info;

    bool get_property_status = controller_->ReadProperty(camera_vector_.at(0), camera_property, current_type);
    bool get_property_info_status = controller_->ReadPropertyInfo(camera_vector_.at(0), camera_property_info, current_type);

    if (!get_property_status || !get_property_info_status || camera_property_info->present == false) {
      RemoveProperty(current_type);
      continue;
    }

    if (map_widgets_.count(current_type) <= 0) {
      continue;
    }

    if (map_widgets_[current_type].check_auto != nullptr) {
      check_auto_signal_mapper_->setMapping(map_widgets_[current_type].check_auto, static_cast<int>(current_type));
      connect(map_widgets_[current_type].check_auto, SIGNAL( stateChanged(int) ), check_auto_signal_mapper_, SLOT( map() ));
    }

    if (map_widgets_[current_type].check_on_off != nullptr) {
      check_on_off_signal_mapper_->setMapping(map_widgets_[current_type].check_on_off, static_cast<int>(current_type));
      connect(map_widgets_[current_type].check_on_off, SIGNAL( stateChanged(int) ), check_on_off_signal_mapper_, SLOT( map() ));
    }

    if (map_widgets_[current_type].slider != nullptr) {
      slider_signal_mapper_->setMapping(map_widgets_[current_type].slider, static_cast<int>(current_type));
      connect(map_widgets_[current_type].slider, SIGNAL( sliderReleased() ), slider_signal_mapper_, SLOT( map() ));
    }

    if (map_widgets_[current_type].spin_box != nullptr) {
      spin_box_signal_mapper_->setMapping(map_widgets_[current_type].spin_box, static_cast<int>(current_type));
      connect(map_widgets_[current_type].spin_box, SIGNAL( valueChanged(int) ), spin_box_signal_mapper_, SLOT( map() ));
    }

  } // for

  connect(timer_, SIGNAL(timeout()), this, SLOT(UpdateWidgets()));
}

void CameraSettingsPage::CreateWidgetMap() {

  for (unsigned int i = 0; i < kNumProps; i++) {
    FlyCapture2::PropertyType type = static_cast<FlyCapture2::PropertyType>(i);       
    WidgetProp widget_prop;
    map_widgets_[type] = widget_prop;
  }

  for (auto &widget : map_widgets_) {
    widget.second.label = nullptr;
    widget.second.label2 = nullptr;
    widget.second.check_auto = nullptr;
    widget.second.check_on_off = nullptr;
    widget.second.slider = nullptr;
    widget.second.spin_box = nullptr;
    widget.second.double_spin_box = nullptr;
  }

}

void CameraSettingsPage::CreatePropertyMap() {

  map_property_[FlyCapture2::BRIGHTNESS] = "Brightness";
  map_property_[FlyCapture2::AUTO_EXPOSURE] = "Exposure";
  map_property_[FlyCapture2::SHARPNESS] = "Sharpness";
  map_property_[FlyCapture2::WHITE_BALANCE] = "WB";
  map_property_[FlyCapture2::HUE] = "Hue";
  map_property_[FlyCapture2::SATURATION] = "Saturation";
  map_property_[FlyCapture2::GAMMA] = "Gamma";
  map_property_[FlyCapture2::IRIS] = "Iris";
  map_property_[FlyCapture2::FOCUS] = "Focus";
  map_property_[FlyCapture2::ZOOM] = "Zoom";
  map_property_[FlyCapture2::PAN] = "Pan";
  map_property_[FlyCapture2::TILT] = "Tilt";
  map_property_[FlyCapture2::SHUTTER] = "Shutter";
  map_property_[FlyCapture2::GAIN] = "Gain";         
  map_property_[FlyCapture2::FRAME_RATE] = "FrameRate";  
  map_property_[FlyCapture2::TEMPERATURE] = "Temperature";  
}

void CameraSettingsPage::GetPropertyWidgets() {

  layout_grid->addWidget(new QLabel("Name"), 0, 1);
  layout_grid->addWidget(new QLabel("Value"), 0, 3);
  layout_grid->addWidget(new QLabel("Auto"), 0, 4);
  for (unsigned int i = 1; i < kNumProps; i++) {

    FlyCapture2::PropertyType current_type = static_cast<FlyCapture2::PropertyType>(i-1);

    if (map_property_.find(current_type) == map_property_.end()) {
      continue;
    }

    std::unique_ptr<FlyCapture2::PropertyInfo> prop_info;
    controller_->ReadPropertyInfo(camera_vector_.at(0), prop_info, current_type);
    unsigned int min_value = prop_info->min;
    unsigned int max_value = prop_info->max;

    // Theese properties belong in their own window!
    if (current_type == FlyCapture2::TRIGGER_MODE || 
        current_type == FlyCapture2::TRIGGER_DELAY || 
        current_type == FlyCapture2::FRAME_RATE) {
      continue;
    }

    if (current_type == FlyCapture2::TEMPERATURE) {
      map_widgets_[current_type].label =  new QLabel(map_property_[current_type].c_str(), this);
      layout_grid->addWidget(map_widgets_[current_type].label, i, 1);
      continue;
    }

    if (current_type == FlyCapture2::WHITE_BALANCE) {
      continue;
    } else {
      map_widgets_[current_type].label = new QLabel(map_property_.find(current_type)->second.c_str(), this);
      layout_grid->addWidget(map_widgets_[current_type].label, i, 1);

      map_widgets_[current_type].slider = new QSlider(Qt::Horizontal, this);
      map_widgets_[current_type].slider->setMinimum(min_value);
      map_widgets_[current_type].slider->setMaximum(max_value);
      layout_grid->addWidget(map_widgets_[current_type].slider, i, 2);

      map_widgets_[current_type].spin_box = new QSpinBox(this);
      map_widgets_[current_type].spin_box->setMinimum(min_value);
      map_widgets_[current_type].spin_box->setMaximum(max_value);
      layout_grid->addWidget(map_widgets_[current_type].spin_box, i, 3);
    }

    map_widgets_[current_type].check_auto = new QCheckBox("", this);
    layout_grid->addWidget(map_widgets_[current_type].check_auto, i, 4);

    //map_widgets_[current_type].check_on_off = new QCheckBox("", this);
    //layout_grid->addWidget(map_widgets_[current_type].check_on_off, i, 5);

  } // for

}

void CameraSettingsPage::UpdateWidgets() {

  for (unsigned int i = 0; i < kNumProps; i++) {

    const FlyCapture2::PropertyType current_type = static_cast<FlyCapture2::PropertyType>(i);

    std::unique_ptr<FlyCapture2::Property> camera_property;
    std::unique_ptr<FlyCapture2::PropertyInfo> camera_property_info;

    bool get_property_status = controller_->ReadProperty(camera_vector_.at(0), camera_property, current_type);
    bool get_property_info_status = controller_->ReadPropertyInfo(camera_vector_.at(0), camera_property_info, current_type);

    if (!get_property_status || !get_property_info_status || camera_property_info->present == false) {
      RemoveProperty(current_type);
      continue;
    }

    if (map_widgets_.count(current_type) <= 0) {
      continue;
    }

    if (map_widgets_[current_type].check_auto != nullptr) {
      UpdateAutoCheckBox(current_type);
    }

    if (map_widgets_[current_type].check_on_off != nullptr) {
      UpdateOnOffCheckBox(current_type);
    }

    if (map_widgets_[current_type].slider != nullptr) {
      UpdateSlider(current_type);
    }

    if (map_widgets_[current_type].spin_box != nullptr) {
      UpdateSpinBox(current_type);
    }

  } // for

}


void CameraSettingsPage::RemoveProperty(FlyCapture2::PropertyType type) {
  if (map_widgets_.count(type) <= 0) {
    return;
  }

  if (map_widgets_.find(type)->second.check_auto != nullptr) {
    map_widgets_.find(type)->second.check_auto->hide();
  }

  if (map_widgets_.find(type)->second.check_on_off != nullptr) {
    map_widgets_.find(type)->second.check_on_off->hide();
  }

  if (map_widgets_.find(type)->second.slider != nullptr) {
    map_widgets_.find(type)->second.slider->hide();
  }
  
  if (map_widgets_.find(type)->second.spin_box != nullptr) {
    map_widgets_.find(type)->second.spin_box->hide();
  }

  if (map_widgets_.find(type)->second.double_spin_box != nullptr) {
    map_widgets_.find(type)->second.double_spin_box->hide();
  }

}

void CameraSettingsPage::UpdateAutoCheckBox(FlyCapture2::PropertyType type) {
  std::unique_ptr<FlyCapture2::Property> camera_property;

  if (controller_->ReadProperty(camera_vector_.at(0), camera_property, type)) {
    map_widgets_[type].check_auto->setChecked(camera_property->autoManualMode);
  }
}

void CameraSettingsPage::UpdateOnOffCheckBox(FlyCapture2::PropertyType type) {
  std::unique_ptr<FlyCapture2::Property> camera_property;

  if (controller_->ReadProperty(camera_vector_.at(0), camera_property, type)) {
    map_widgets_[type].check_on_off->setChecked(camera_property->onOff);
  }
}

void CameraSettingsPage::UpdateSlider(FlyCapture2::PropertyType type) {
  std::unique_ptr<FlyCapture2::Property> camera_property;

  if (controller_->ReadProperty(camera_vector_.at(0), camera_property, type)) {
    map_widgets_[type].slider->setValue(camera_property->valueA);
  }
}

void CameraSettingsPage::UpdateSpinBox(FlyCapture2::PropertyType type) {
  std::unique_ptr<FlyCapture2::Property> camera_property;

  if (controller_->ReadProperty(camera_vector_.at(0), camera_property, type)) {
    map_widgets_[type].spin_box->setValue(camera_property->valueA);
  }
}

void CameraSettingsPage::SliderChanged(int id) {
  FlyCapture2::PropertyType type = static_cast<FlyCapture2::PropertyType>(id);

  int value = static_cast<QSlider*>( slider_signal_mapper_->mapping(id) )->value();

  std::unique_ptr<FlyCapture2::Property> camera_property;

  controller_->ReadProperty(camera_vector_.at(0), camera_property, type);

  if (camera_property != nullptr) {
    camera_property->type = type;
    camera_property->valueA = value;
    controller_->SetProperty(camera_vector_.at(0), camera_property);
  }

  UpdateWidgets();
}

void CameraSettingsPage::AutoCheckBoxChanged(int id) {
  FlyCapture2::PropertyType type = static_cast<FlyCapture2::PropertyType>(id);

  QCheckBox *checkBox = static_cast<QCheckBox*>( check_auto_signal_mapper_->mapping(id) );

  for (auto &it : camera_vector_) {
    std::unique_ptr<FlyCapture2::Property> camera_property;
    controller_->ReadProperty(it, camera_property, type);

    if (camera_property != nullptr) {
      camera_property->autoManualMode = checkBox->isChecked();
      controller_->SetProperty(it, camera_property);
    }
  }

  UpdateWidgets();
}

void CameraSettingsPage::OnOffCheckBoxChanged(int id) {
  FlyCapture2::PropertyType type = static_cast<FlyCapture2::PropertyType>(id);

  QCheckBox *checkBox = static_cast<QCheckBox*>( check_on_off_signal_mapper_->mapping(id) );

  for (auto &it : camera_vector_) {
    std::unique_ptr<FlyCapture2::Property> camera_property;
    controller_->ReadProperty(it, camera_property, type);

    if (camera_property != nullptr) {
      camera_property->onOff = checkBox->isChecked();
      controller_->SetProperty(it, camera_property);
    }
  }

  UpdateWidgets();
}

void CameraSettingsPage::SpinBoxChanged(int id) {
  FlyCapture2::PropertyType type = static_cast<FlyCapture2::PropertyType>(id);

  QSpinBox *spin_box = static_cast<QSpinBox*>( spin_box_signal_mapper_->mapping(id) );

  std::unique_ptr<FlyCapture2::Property> camera_property;
  std::unique_ptr<FlyCapture2::PropertyInfo> camera_property_info;
  controller_->ReadProperty(camera_vector_.at(0), camera_property, type);
  controller_->ReadPropertyInfo(camera_vector_.at(0), camera_property_info, type);

  if (spin_box->value() < static_cast<int>(camera_property_info->min)) {
    spin_box->setValue(camera_property_info->min);
    
  } else if (spin_box->value() > static_cast<int>(camera_property_info->max)) {
    spin_box->setValue(camera_property_info->max);

  }

  if (camera_property != nullptr) {
    camera_property->valueA = spin_box->value();
    for (auto &it : camera_vector_) {
      controller_->SetProperty(it, camera_property);
    }
  }

  UpdateWidgets();
}

void CameraSettingsPage::NameLineChanged() {
  if (camera_vector_.size() > 1) {
    return;
  }
  QString q_str = line_edit_camera_name->displayText();
  controller_->ChangeCameraName(camera_vector_.at(0), q_str.toStdString());
  main_window_->SetCameraName(camera_vector_.at(0), q_str);
}
