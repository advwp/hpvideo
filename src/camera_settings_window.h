#ifndef CAMERA_SETTINGS_WINDOW_H
#define CAMERA_SETTINGS_WINDOW_H

#include "ui_camera_settings_window.h"

#include "main_window.h"

#include <memory>

class Controller;

/* \brief Class controlling multiple pages. 
* 
* This is the parent of all the  pages used for setting 
* options on a camera. 
*/
class CameraSettingsWindow : public QWidget, private Ui::win_camera_settings {

  Q_OBJECT

public: 
  
  /*! \brief Constructor
   * 
   * @param controller
   * @param camera_vector selected cameras. 
   * @param parent parent widget
   */
  CameraSettingsWindow(std::shared_ptr<Controller> controller, 
                       std::vector<unsigned int> &camera_vector, 
                       QWidget *parent = 0, 
                       MainWindow *main_window = nullptr);

public slots:

private: 

  /*! \brief Initial setup
  */
  void Init();

  /*! \brief Connects signals
  */
  void ConnectSignals();

  MainWindow *main_window_;

  std::shared_ptr<Controller> controller_; //!< Controller
  std::vector<unsigned int> camera_vector_; //!< Selected cameras
  unsigned int serial_number_; //!< First selected camera

  int settings_page_num_;
  int info_page_num_;
  int adv_settings_page_num_;
  int trigger_page_num_;
  int video_modes_page_num_;
};

const QString kSettingsPageLabel = "Camera settings";
const QString kInfoPageLabel = "Camera information";
const QString kVideoModesPageLabel = "Video modes";
const QString kAdvSettingsPageLabel = "Advanced settings";
const QString kTriggerPageLabel = "Trigger modes";

#endif
