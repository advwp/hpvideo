#include <QtGui>
#include "takes_window.h"
#include "save_take_window.h"
#include "database.h"
#include "logger.h"

#include "controller.h"

#include <vector>

using std::shared_ptr;
using std::vector;

TakesWindow::TakesWindow(shared_ptr<Controller> controller, 
                         QWidget* /* UNUSED */)  {

  setupUi(this);

  this->controller_ = controller;

  Init();
}

void TakesWindow::Init() {
  tree_take_list->setColumnCount(5);
  QStringList lbl_take_list;
  lbl_take_list << "Take ID" << "Start date" << "End date" << "Num cameras" << "FPS";
  tree_take_list->setHeaderLabels(lbl_take_list);

  ConnectSignals();
  UpdateTakesList();
}

void TakesWindow::AddTakeToList(const QString &take_id, 
                                const QString &start_date, 
                                const QString &end_date, 
                                const QString &num_cameras, 
                                const QString &fps) {
    
  QTreeWidgetItem *itm = new QTreeWidgetItem(tree_take_list);

  itm->setText(0, take_id);
  itm->setText(1, start_date);
  itm->setText(2, end_date);
  itm->setText(3, num_cameras);
  itm->setText(4, fps);

  tree_take_list->addTopLevelItem(itm);
}

void TakesWindow::ConnectSignals() {

  connect(btn_save_take, SIGNAL( clicked() ), this, SLOT( BtnSaveTakeClicked() ));
  connect(btn_delete_take, SIGNAL( clicked() ), this, SLOT( BtnDeleteTakeClicked() ));
}

void TakesWindow::UpdateTakesList() {
  Database database(DB_NAME);

  tree_take_list->clear();

  vector<TakeStruct> takes;

  if (database.GetTakes(takes) != DB_OK) {
    return;
  }

  for (auto &take : takes) {
    QString take_id;
    take_id.setNum(take.take_id, 10);
    QString start_date(take.start_date.c_str());
    QString end_date(take.end_date.c_str());
    QString num_cameras;
    num_cameras.setNum(take.num_cameras, 10);
    QString fps;
    fps.setNum(take.fps, 'f', 2);
    this->AddTakeToList(take_id, start_date, end_date, num_cameras, fps);
  }
}

void TakesWindow::BtnSaveTakeClicked() {
  int take_id = -1;
  float fps = -1.0;
  GetSelection(take_id, fps);
  if (take_id >= 0) {
          SaveTakeWindow *win = new SaveTakeWindow(controller_, this);
          win->SetSelection(take_id, fps);
          win->show();
  } else {
          return;       
  }
}

void TakesWindow::BtnDeleteTakeClicked() {

  Database database(DB_NAME);

  int take_id = -1;
  float fps = -1.0;
  GetSelection(take_id, fps);

  if (take_id >= 0) {

    switch( QMessageBox::information( this, "Confirm delete",
                                     "Are you sure you want to delete take?",
                                     "Delete", "Cancel", 0, 1 ) ) {
      case 0:
        database.DeleteTake(take_id);
        UpdateTakesList();
        break;
      case 1:
      default:
        break;
    }
  }
}

void TakesWindow::GetSelection(int &take_id, float &fps) {
  
  QList<QTreeWidgetItem *> list = tree_take_list->selectedItems();

  bool ok_take_id = false;
  bool ok_fps = false;

  if (list.size() <= 0) {
          take_id = -1;
          fps = -1.0;
          return;
  }

  int temp_take_id = list.at(0)->text(0).toInt(&ok_take_id, 10);
  float temp_fps = list.at(0)->text(4).toFloat(&ok_fps);

  if (ok_take_id && ok_fps) {
          take_id = temp_take_id;
          fps = temp_fps;
  } else {
          take_id = -1;
          fps = -1.0;
  }
}
