#include "logger.h"

#include "static_info.h"
#include "global_settings.h"

#include <iostream>

using std::ofstream;
using std::string;

void Logger::Log(const LOG_LEVEL lvl, const string message) {
        if (GlobalSettings::GetVariable(static_cast<GLOBAL_VARIABLES>((int)lvl)) > 0) {
          std::fstream file_stream;
          file_stream.open (kLogFileName.c_str(), std::fstream::out | std::fstream::app);
          file_stream << message << std::endl;
          file_stream.close();
        }
}

void Logger::Log(const LOG_LEVEL lvl, const int error_code, const string message) {
        if (GlobalSettings::GetVariable(static_cast<GLOBAL_VARIABLES>((int)lvl)) > 0) {
          std::fstream file_stream;
          file_stream.open (kLogFileName.c_str(), std::fstream::out | std::fstream::app);
          file_stream << error_code << " - " << message << std::endl;
          file_stream.close();
        }
}

#ifndef WIN32
void Logger::PrintTimeDifference(const timeval &tv1, const timeval &tv2, const string &name) {

        double elapsed_time;

        elapsed_time = (tv2.tv_sec - tv1.tv_sec) * 1000.0;
        elapsed_time += (tv2.tv_usec - tv1.tv_usec) / 1000.0;
        
        std::fstream file_stream;
        file_stream.open (kLogFileName.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
        file_stream << elapsed_time << "ms. (" << name << ")" << std::endl;
        file_stream.close();
}
#endif
