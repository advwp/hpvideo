#ifndef VIDEO_MODES_PAGE_H
#define VIDEO_MODES_PAGE_H

#include "ui_video_modes_page.h"

#include "FlyCapture2.h"

#include <memory>
#include <map>
#include <vector>
#include <QtGui>

class Controller;

/*! \brief Class controlling the video modes page. 
*/
class VideoModesPage : public QWidget, private Ui::video_modes_page {

  Q_OBJECT

public:

  /*! \brief Constructor
   * 
   * @param controller
   * @param camera_vector selected cameras. 
   * @param parent parent widget
   */
  VideoModesPage(std::shared_ptr<Controller> controller, 
                 std::vector<unsigned int> &camera_vector, 
                 QWidget *parent = 0);

public slots:
  void VideoModeButtonClicked(int mode);
  void FrameRateButtonClicked(int rate);

private: 

  /*! \brief Initial setup. 
   */
  void Init();

  /*! \brief Create maps used as local variables. 
   */
  void SetupMaps();

  /*! \brief Connects signals. 
   */
  void ConnectSignals();

  /*! \brief Hide unsupported options. 
  */
  void SetupButtonWidgets();

  /*! \brief Hides unsupported frame rates. 
   */
  void UpdateFrameRates(FlyCapture2::VideoMode video_mode);

  std::map<FlyCapture2::VideoMode, QRadioButton*> video_mode_button_map_;
  std::map<FlyCapture2::FrameRate, QRadioButton*> frame_rate_button_map_;

  std::vector<unsigned int> camera_vector_; //!< Selected cameras

  std::shared_ptr<Controller> controller_; //!< Controller

  QSignalMapper *video_mode_signal_mapper_; //!< Internal signal mapper. 
  QSignalMapper *frame_rate_signal_mapper_; //!< Internal signal mapper.

};

#endif
