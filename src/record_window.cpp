#include <QtGui>
#include <sstream>
#include <string>

#include "record_window.h"
#include "controller.h"

#include "static_info.h"
#include "global_settings.h"

#include "logger.h"

using std::vector;
using std::shared_ptr;
using std::string;

RecordWindow::RecordWindow(shared_ptr<Controller> controller, 
               vector<unsigned int> &camera_vector, 
               QWidget* /* UNUSED */) {
  setupUi(this);

  this->controller_ = controller;

  timer_ = new QTimer(this);
  trigger_timer_ = new QTimer(this);

  for (auto serial_number : camera_vector) {
    camera_vector_.push_back(serial_number);
  }

  Init();
}

RecordWindow::~RecordWindow() {
  BtnStopRecordClicked();
}

void RecordWindow::Init() {

  btn_stop_record->setEnabled(false);
  tree_camera_list->setColumnCount(2);
  QStringList lbl_camera_list;
  lbl_camera_list << "Name" << "Serial number";
  tree_camera_list->setHeaderLabels(lbl_camera_list);

  QDoubleValidator *val = new QDoubleValidator(1.0, 120.0, 1, fps_edit);
  fps_edit->setValidator(val);
  fps_edit->setText("30.0");

  progress_take_save->setValue(0);

  ConnectSignals();

  SetCameras(camera_vector_);

  BtnSaveImagesClicked();
  btn_redirect_images->setDown(true);
}

void RecordWindow::SetCameras(vector<unsigned int> &cameras) {

  this->selection_.clear();

  for (auto serial_number : cameras) {

      this->selection_.push_back(serial_number);
      QTreeWidgetItem *item = new QTreeWidgetItem(tree_camera_list);

      string name;
      if (controller_->GetCameraName(serial_number, name)) {
        QString q_str_name(name.c_str());
        item->setText(0, q_str_name);
      }

      QString q_str_serial_number;
      q_str_serial_number = q_str_serial_number.setNum(serial_number, 10);
      item->setText(1, q_str_serial_number);
      tree_camera_list->addTopLevelItem(item);

  }

}

void RecordWindow::ConnectSignals() {
  connect(btn_start_record, SIGNAL( clicked() ), this, SLOT( BtnStartRecordClicked() ));
  connect(btn_stop_record, SIGNAL( clicked() ), this, SLOT( BtnStopRecordClicked() ));

  connect(btn_save_images, SIGNAL( clicked() ), this, SLOT( BtnSaveImagesClicked() ));
  connect(btn_redirect_images, SIGNAL( clicked() ), this, SLOT( BtnTransferImagesClicked() ));

  connect(timer_, SIGNAL(timeout()), this, SLOT(UpdateProgressBar()));
  connect(trigger_timer_, SIGNAL(timeout()), this, SLOT( FireSoftwareTriggers() ));

}

void RecordWindow::BtnStartRecordClicked() {

  bool ok = false;
  double fps = fps_edit->text().toDouble(&ok);
  if (!ok) {
    return;
  }

  selection_.clear();
  
  QTreeWidgetItemIterator it(tree_camera_list);
  while (*it) {
    bool ok = false;
    unsigned int tmp_serial;
    tmp_serial = (*it)->text(1).toUInt(&ok, 10);
    if (ok) {
      selection_.push_back(tmp_serial);
    }
    ++it;
  }

  for (auto &it : selection_) {
    if (!controller_->SetTriggerOnOff(it, false)) {
      return;
    }
  }

  int ms = -1;
  if (checkbox_software_trigger->isChecked()) {
    if (!ok) {
      return;
    } else {
      ms = int(((double)1 / fps) * (double)1000);
    }
  }

  // Start record.
  if (!controller_->StartCapture(selection_, fps)) {
    return;
  }

  if (checkbox_software_trigger->isChecked()) {
    trigger_timer_->start(ms);
  }

  lbl_status->setText("Capturing");
  btn_start_record->setEnabled(false);
  btn_stop_record->setEnabled(true);

}

void RecordWindow::BtnStopRecordClicked() {

  btn_stop_record->setEnabled(false);

  trigger_timer_->stop();
  // Stop record.
  controller_->StopCapture();

  lbl_status->setText("Saving images");
  progress_take_save->setMinimum(0);
  progress_take_save->setMaximum(controller_->GetMaxNumberOfImagesRemaining());

  timer_->start(500);
}

void RecordWindow::BtnSaveImagesClicked() {
  GlobalSettings::SetVariable(STORE_IMAGES, 1);
}

void RecordWindow::BtnTransferImagesClicked() {
  GlobalSettings::SetVariable(STORE_IMAGES, 0);
}

void RecordWindow::UpdateProgressBar() {
  int i = controller_->GetNumberOfImagesRemaining();
  if (i < 1) {
    timer_->stop();
    lbl_status->setText("Idle");
    btn_start_record->setEnabled(true);
    progress_take_save->setValue(0);
    return;
  }
  progress_take_save->setValue(i);
}

void RecordWindow::FireSoftwareTriggers() {
  for (auto &it : selection_) {
    controller_->FireSoftwareTrigger(it);
  }
}
