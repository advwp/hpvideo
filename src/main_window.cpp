#include <QtGui>

#include <sstream>
#include <map>

#include "main_window.h"
#include "takes_window.h"
#include "record_window.h"
#include "live_view_window.h"
#include "camera_settings_window.h"
#include "program_settings.h"
#include "static_info.h"

#include "camera_con_local.h"

#include "controller.h"

using std::map;
using std::shared_ptr;
using std::make_shared;
using std::vector;
using std::stringstream;

MainWindow::MainWindow(QWidget* /*unused*/) {

  controller_ = make_shared<Controller>();
  setupUi(this);

  Init();
}

void MainWindow::Init() {
  tree_camera_list->setColumnCount(5);
  tree_camera_list->setSelectionMode(QAbstractItemView::MultiSelection);
  QStringList lbl_camera_list;
  lbl_camera_list << "Name" << "Serial" << "Model" << "Interface" << "IP";
  tree_camera_list->setHeaderLabels(lbl_camera_list);

  this->setAttribute(Qt::WA_DeleteOnClose);

  ConnectSignals();
  BtnRefreshClicked();
}

void MainWindow::ConnectSignals() {
  connect(btn_refresh, SIGNAL( clicked() ), this, SLOT( BtnRefreshClicked() ));
  connect(btn_configure_selected, SIGNAL( clicked() ), this, SLOT( BtnConfigureSelectedClicked() ));
  connect(btn_live_view, SIGNAL( clicked() ), this, SLOT( BtnLiveViewClicked() ));
  connect(btn_record, SIGNAL( clicked() ), this, SLOT( BtnRecordClicked() ));
  connect(btn_takes, SIGNAL( clicked() ), this, SLOT( BtnTakesClicked() ));
  connect(btn_program_settings, SIGNAL( clicked() ), this, SLOT( BtnProgramSettingsClicked() ));
}

void MainWindow::AddCameraToList(const QString name, 
                                 const QString serial, 
                                 const QString model, 
                                 const QString interface, 
                                 const QString ip) {

  QTreeWidgetItem *item = new QTreeWidgetItem(tree_camera_list);

  item->setText(0, name);
  item->setText(1, serial);
  item->setText(2, model);
  item->setText(3, interface);
  item->setText(4, ip);

  tree_camera_list->addTopLevelItem(item);
}

void MainWindow::BtnRefreshClicked() {

  controller_->UpdateCameras();

  tree_camera_list->clear();

  map<unsigned int, shared_ptr<CameraConLocal>> *all_cameras = controller_->all_cameras();

  for (auto &current_camera : *all_cameras) {
    auto s = current_camera.first;
    stringstream out;
    out << s;
    unsigned int serial_number = (unsigned int)atoi(out.str().c_str());
    QString name;
    std::string stdname;
    if (controller_->GetCameraName(serial_number, stdname)) {
      name = stdname.c_str();
    } else {
      name = "";
    }
    QString serial(out.str().c_str());
    QString model(current_camera.second->GetModel().c_str());
    QString type;
    QString ip;

    if (current_camera.second->GetType() == LOCAL) {
      type = "Local";
      ip = "N/A";
    } else {
      type = "Network";
      ip = "";
    }

    AddCameraToList(name, serial, model, type, ip);
  }
  
}

void MainWindow::BtnConfigureSelectedClicked() {
  auto selections = GetSelections();

  if (selections.size() < 1) {
    return;
  }

  auto *win = new CameraSettingsWindow(controller_, selections, this, this);
  win->show();
}

void MainWindow::BtnLiveViewClicked() {
  auto selections = GetSelections();
  
  QPoint pos(0, 0);
  QSize size(300, 300);
  for (auto it : selections) {
    auto *win = new LiveViewWindow(controller_, it, size, pos, this);
    win->show();
    win->move(pos);
    GetNextSizeAndPos(size, pos);
  }

}

void MainWindow::BtnRecordClicked() {
  auto selections = GetSelections();

  if (selections.size() < 1) {
    return;
  }

  auto *win = new RecordWindow(controller_, selections, this);
  win->show();
}

void MainWindow::BtnTakesClicked() {

  auto *win = new TakesWindow(controller_, this);
  win->show();
}

void MainWindow::BtnProgramSettingsClicked() {
  auto *win = new ProgramSettings(this);
  win->show();
}

vector<unsigned int> MainWindow::GetSelections() {
  vector<unsigned int> selections;

  QList<QTreeWidgetItem *> list = tree_camera_list->selectedItems();

  for (int i = 0; i < list.size(); i++) {
    bool ok;
    selections.push_back(list.at(i)->text(1).toUInt(&ok, 10));
  }

  return selections;
}

void MainWindow::SetCameraName(const unsigned int serial_number, const QString &name) {
  QString q_str_serial_number;
  q_str_serial_number.setNum(serial_number, 10);
  QTreeWidgetItemIterator it(tree_camera_list);
  while (*it) {
    if ((*it)->text(1) == q_str_serial_number) {
      (*it)->setText(0, name);
      break;
    }
    ++it;
  }
}

void MainWindow::GetNextSizeAndPos(QSize &size, QPoint &pos) {
  if (pos.x() > ((3 * size.width()) - 10)) {
    pos.setY(pos.y() + (size.height() + 10));
    pos.setX(0);
  } else {
    pos.setX(pos.x() + size.width() + 10);
  }
}
