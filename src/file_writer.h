#ifndef FILE_WRITER_H
#define FILE_WRITER_H

#include <vector>
#include <sstream>
#include <memory>

#include "FlyCapture2.h"

/*! \brief Class for writing a series of images to files. 
* 
* This class is used by the Controller class to store 
* takes on disk. 
*/
class FileWriter {

public:

  /*! \brief Write images to AVI
   * 
   * This uses the FlyCapture2 API to store images in AVI format. 
   * @param serial_number
   * @param take_id 
   * @param frame_rate frame rate of the take. 
   * @param images images to store. 
   */
  void WriteToAvi(const int serial_number, 
                  const int take_id, 
                  const float frame_rate, 
                  const std::vector<std::unique_ptr<FlyCapture2::Image>> &images);

  /*! \brief Write images to JPEG
   * 
   * This uses the FlyCapture2 API to store images in JPEG format. 
   * Warning: This can mean a LOT of individual JPEGS are stored. 
   * @param serial_number
   * @param take_id
   * @param images images to store
   */
  void WriteToJpeg(const int serial_number, 
                   const int take_id, 
                   const std::vector<std::unique_ptr<FlyCapture2::Image>> &images);

private:

  FlyCapture2::AVIRecorder avi_recorder_; //!< Avi recorder object. 

}; // FileWriter

#endif // FILE_WRITER_H
