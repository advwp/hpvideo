#include "program_settings.h"

#include "global_settings.h"
#include "database.h"

#include <QtGui>

ProgramSettings::ProgramSettings(QWidget* /* UNUSED */) {
  setupUi(this);
  
  Init();
}

void ProgramSettings::Init() {

  if (GlobalSettings::GetVariable(LOG_CAM_INFO) == 1) {
    GlobalSettings::SetVariable(LOG_DB_INFO, 1);
    check_log_info->setChecked(true);
  } else if (GlobalSettings::GetVariable(LOG_DB_INFO) == 1) {
    GlobalSettings::SetVariable(LOG_CAM_INFO, 1);
    check_log_info->setChecked(true);
  } else {
    check_log_info->setChecked(false);
  }

  if (GlobalSettings::GetVariable(LOG_CAM_CRIT) == 1) {
    GlobalSettings::SetVariable(LOG_DB_CRIT, 1);
    check_log_crit->setChecked(true);
  } else if (GlobalSettings::GetVariable(LOG_DB_CRIT) == 1) {
    GlobalSettings::SetVariable(LOG_CAM_CRIT, 1);
    check_log_info->setChecked(true);
  } else {
    check_log_info->setChecked(false);
  }
  
  ConnectSignals();
} 

void ProgramSettings::ConnectSignals() {
  connect(check_log_info, SIGNAL ( clicked(bool) ), this, SLOT( CheckLogInfoClicked(bool) ) );
  connect(check_log_crit, SIGNAL ( clicked(bool) ), this, SLOT( CheckLogCritClicked(bool) ) );

  connect(btn_clear_database, SIGNAL ( clicked() ), this, SLOT( BtnClearDatabaseClicked() ) );
}

void ProgramSettings::CheckLogInfoClicked(bool state) {
  if (state) {
    GlobalSettings::SetVariable(LOG_DB_INFO, 1);
    GlobalSettings::SetVariable(LOG_CAM_INFO, 1);
  } else {
    GlobalSettings::SetVariable(LOG_DB_INFO, 0);
    GlobalSettings::SetVariable(LOG_CAM_INFO, 0);
  }
}

void ProgramSettings::CheckLogCritClicked(bool state) {
  if (state) {
    GlobalSettings::SetVariable(LOG_DB_CRIT, 1);
    GlobalSettings::SetVariable(LOG_CAM_CRIT, 1);
  } else {
    GlobalSettings::SetVariable(LOG_DB_CRIT, 0);
    GlobalSettings::SetVariable(LOG_DB_CRIT, 0);
  }
}

void ProgramSettings::BtnClearDatabaseClicked() {
    Database database(DB_NAME);
    switch( QMessageBox::information( this, "Confirm clear database",
                                     "Are you sure you want to clear the entire database?",
                                     "Delete", "Cancel", 0, 1 ) ) {
      case 0:
        database.ClearDatabase();
        QMessageBox::information(this, "Ok", "Database cleared!", 
                                  "Ok");
        break;
      case 1:
      default:
        break;
    }

}
