#include <QtGui>

#include "camera_trigger_page.h"

#include "controller.h"

CameraTriggerPage::CameraTriggerPage(std::shared_ptr<Controller> controller, 
                                     std::vector<unsigned int> &camera_vector, 
                                     QWidget *parent) : QWidget(parent) {

  setupUi(this);

  this->controller_ = controller;

  for (auto serial_number : camera_vector) {
    this->camera_vector_.push_back(serial_number);
  }

  Init();
}

void CameraTriggerPage::Init() {
  ConnectSignals();

  if (camera_vector_.size() == 1) {
    bool trigger = false;
    if (controller_->GetTriggerOnOff(camera_vector_.at(0), trigger)) {
      btn_enable_trigger->setChecked(trigger);
    }
  }
  radio_gpio_0->toggle();
}

void CameraTriggerPage::ConnectSignals() {
  connect(btn_enable_trigger, SIGNAL( clicked() ), this, SLOT( BtnEnableTriggerClicked() ));
  connect(btn_fire_software_trigger, SIGNAL( clicked() ), this, SLOT( BtnFireSoftwareTriggerClicked() ));
}

void CameraTriggerPage::BtnEnableTriggerClicked() {
  for (auto &it : camera_vector_) {
    controller_->SetTriggerOnOff(it, btn_enable_trigger->isChecked());
  }
}

void CameraTriggerPage::BtnFireSoftwareTriggerClicked() {
  for (auto &it : camera_vector_) {
    controller_->FireSoftwareTrigger(it);
  }
}
