#ifndef LOGGER_H
#define LOGGER_H

#include <fstream>
#include <string>

#include "static_info.h"

#ifdef WIN32
#include <time.h>
#else
#include <sys/time.h>
#endif

/*! \brief Class used for logging. 
*/
class Logger {

public:
    /*! \brief Log a message.
     * 
     * @param lvl log level
     * @param message message to log
     */
    static void Log(const LOG_LEVEL lvl, const std::string message);

    /*! \brief Log a message with an error code. 
     * 
     * @param lvl log level
     * @param error_code 
     * @param message
     */
    static void Log(const LOG_LEVEL lvl, int error_code, std::string message);

#ifndef WIN32
    /*! \brief Log a time difference. 
     * 
     * @param tv1 first timeval 
     * @param tv2 second timeval 
     * @param name associated. 
     */
    static void PrintTimeDifference(const timeval &tv1, 
                                    const timeval &tv2, 
                                    const std::string &name);
#endif

}; // Logger

#endif // LOGGER_H
