#include <QtGui>

#include "camera_settings_window.h"

#include "camera_settings_page.h"
#include "camera_adv_settings_page.h"
#include "camera_info_page.h"
#include "video_modes_page.h"

#include "controller.h"


CameraSettingsWindow::CameraSettingsWindow(std::shared_ptr<Controller> controller, 
                                           std::vector<unsigned int> &camera_vector, 
                                           QWidget* /* UNUSED */, 
                                           MainWindow *main_window) {

  setupUi(this);

  main_window_ = main_window;

  //this->setFixedSize(550, 500);
  QSize size(550, 480);      
  this->resize(size);
  
  this->setAttribute( Qt::WA_DeleteOnClose );

  for (auto serial_number : camera_vector) {
    camera_vector_.push_back(serial_number);
  }

  this->controller_ = controller;

  Init();

}

void CameraSettingsWindow::Init() {

  tab_widget->clear();

  auto *settings_page = new CameraSettingsPage(controller_, camera_vector_, tab_widget, main_window_);
  settings_page_num_ = tab_widget->addTab(settings_page, kSettingsPageLabel);

  if (camera_vector_.size() == 1) {
    auto *info_page = new CameraInfoPage(controller_, camera_vector_, tab_widget);
    info_page_num_ = tab_widget->addTab(info_page, kInfoPageLabel);
  }

  auto *video_modes_page = new VideoModesPage(controller_, camera_vector_, tab_widget);
  video_modes_page_num_ = tab_widget->addTab(video_modes_page, kVideoModesPageLabel);

  auto *adv_settings_page = new CameraAdvSettingsPage(controller_, camera_vector_, tab_widget);
  adv_settings_page_num_ = tab_widget->addTab(adv_settings_page, kAdvSettingsPageLabel);

}
