#ifndef CAMERA_SETTINGS_PAGE_H
#define CAMERA_SETTINGS_PAGE_H

#include "ui_camera_settings_page.h"

#include "main_window.h"

#include "FlyCapture2.h"

#include <string>
#include <memory>
#include <mutex>
#include <QtGui>

class Controller;

struct WidgetProp;

/*! \brief Class defining the camera settings page. 
*/
class CameraSettingsPage : public QWidget, private Ui::camera_settings_page {

  Q_OBJECT

public:

  /*! \brief Constructor
   * 
   * @param controller
   * @param camera_vector selected cameras. 
   * @param parent parent widget
   */
  CameraSettingsPage(std::shared_ptr<Controller> controller, 
                     std::vector<unsigned int> &camera_vector, 
                     QWidget *parent = 0, MainWindow *main_window = nullptr);

  /*! \brief Destructor
  */
  ~CameraSettingsPage();

public slots:
  
  /*! \brief Updates name when name line is changed.
   */
  void NameLineChanged();

  /*! \brief Called when a slider is updated. 
   * 
   * Sets the new value on the camera. 
   */
  void SliderChanged(int id);

  /*! \brief Called when a auto checkbox is updated. 
   *
   * Sets the new value on the camera. 
   */
  void AutoCheckBoxChanged(int id);

  /*! \brief Called when a on/off checkbox is updated. 
   * 
   * Sets the new value on the camera. 
   */
  void OnOffCheckBoxChanged(int id);

  /*! \brief Called when a spin box is update.
   *
   * Sets the new value on the camera. 
   */
  void SpinBoxChanged(int id);

  /*! \brief Updates all widgets to the current camera settings. 
   */
  void UpdateWidgets();

private:

  /*! \brief Initial setup. 
   */
  void Init();

  /*! \brief Connect signals. 
   */
  void ConnectSignals();

  /*! \brief Creates map of available properties. 
   */
  void CreatePropertyMap();

  /*! \brief Adds property widgets. 
   */
  void GetPropertyWidgets();

  /*! \brief Creates map of all widgets. 
   */
  void CreateWidgetMap();

  /*! \brief Update to match camera value. 
   *
   * @param type
   */
  void UpdateAutoCheckBox(FlyCapture2::PropertyType type);

  /*! \brief Update to match camera value. 
   *
   * @param type
   */
  void UpdateOnOffCheckBox(FlyCapture2::PropertyType type);

  /*! \brief Update to match camera value. 
   *
   * @param type
   */
  void UpdateSlider(FlyCapture2::PropertyType type);

  /*! \brief Update to match camera value. 
   *
   * @param type
   */
  void UpdateSpinBox(FlyCapture2::PropertyType type);

  /*! \brief Remove controllers for a property. 
   * 
   * Hides properties not supported on the connected camera. 
   * @param type
   */
  void RemoveProperty(FlyCapture2::PropertyType type);

  std::map<FlyCapture2::PropertyType, std::string> map_property_; //!< Names
  std::map<FlyCapture2::PropertyType, WidgetProp> map_widgets_; //!< Widgets

  std::shared_ptr<Controller> controller_; //!< Controller

  std::vector<unsigned int> camera_vector_; //!< Selected cameras

  QTimer *timer_; //!< Timer for updating camera values. 

  QSignalMapper *slider_signal_mapper_; //!< Internal signal mapper
  QSignalMapper *check_auto_signal_mapper_; //!< Internal signal mapper
  QSignalMapper *check_on_off_signal_mapper_; //!< Internal signal mapper
  QSignalMapper *spin_box_signal_mapper_; //!< Internal signal mapper

  MainWindow *main_window_;

};

const unsigned int kNumProps = 18; //!< Max number of properties

/*! \brief Struct used for storing widgets
*/
struct WidgetProp {
  QLabel *label;
  QLabel *label2;
  QCheckBox *check_auto;
  QCheckBox *check_on_off;
  QSlider *slider;
  QSpinBox *spin_box;
  QDoubleSpinBox *double_spin_box;
};

#endif
