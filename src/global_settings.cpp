#include "global_settings.h"

#include <fstream>
#include <string>
#include <vector>

using namespace std;

const string GlobalSettings::GLOBAL_VARIABLES_NAMES[] = { 
    "LOG_CAM_INFO", 
    "LOG_CAM_CRIT", 
    "LOG_DB_INFO", 
    "LOG_DB_CRIT", 
    "STORE_IMAGES", 
    "USE_SOFTWARE_TRIGGER", 
    "NOT_FOUND"
};


void GlobalSettings::SetVariable(const GLOBAL_VARIABLES var, const int val) {
  string line;
  vector<string> lines;
  bool found = false;

  // Search the settings file first. 
  ifstream in(kSettingsFileName.c_str());
  if (in.is_open()) {
    while (in.good()) {
      getline(in, line);
      if (line.size() > 1) {
        lines.push_back(line);

        string first_part = line.substr(0, line.find_first_of(' ', 0));
        if (first_part.compare(GlobalSettings::GLOBAL_VARIABLES_NAMES[var]) == 0) {
          found = true;
        }

      }

    }
  }
  in.close();

  fstream out;
  // Append variable if it's not found and return.   
  if (!found) {
    out.open(kSettingsFileName.c_str(), ios::app | ios::out);
    out << GLOBAL_VARIABLES_NAMES[var] << " " << val << "\n";
    out.close();
    return;
  }

  out.open(kTmpSettingsFileName.c_str(), ios::out);
  // Replace the entire file, with the changed variable. 
  for (auto &i : lines) {
    
      string first_part = i.substr(0, i.find_first_of(' '));
      if (first_part.compare(GlobalSettings::GLOBAL_VARIABLES_NAMES[var]) == 0) {
        out << GLOBAL_VARIABLES_NAMES[var] << " " << val << "\n";
      } else {
        out << i << "\n";
      }
  }
  out.close();

  remove(kSettingsFileName.c_str());
  rename(kTmpSettingsFileName.c_str(), kSettingsFileName.c_str());
}

int GlobalSettings::GetVariable(const GLOBAL_VARIABLES var) {
  string line;
  int ret = -1;

  ifstream in(kSettingsFileName.c_str());
  if (in.is_open()) {
    while (in.good()) {
      getline(in, line);

      if (line.size() > 1) {
        string first_part = line.substr(0, line.find_first_of(' '));

        if (first_part.compare(GlobalSettings::GLOBAL_VARIABLES_NAMES[var]) == 0) {
          string second_part = line.substr(line.find_first_of(' ')+1, 1);
          ret = atoi(second_part.c_str());
        }
      }

    }
    in.close();
  }
  return ret;
}
