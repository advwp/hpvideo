#ifndef CAMERA_CON_H
#define CAMERA_CON_H

#include "FlyCapture2.h"

#include <string>
#include <memory>

#include "static_info.h"

class Camera;
/*! \brief Parent class of CameraConLocal and CameraConRemote
* 
* This class is the parent class of CameraConLocal and CameraConRemote. 
* All functions have their own implementations in the child classes. 
* Most of the functions correspond to their respective functions 
* in camera.h. Extra functions are documented where they appear. 
*/
class CameraCon {

public: 

  virtual ~CameraCon() {}

  /* \brief Get the type of the camera. 
   * 
   * @return type of camera. 
   */
  virtual CAM_INTERFACE_TYPE GetType() = 0;

  virtual bool GetNewImage(std::unique_ptr<FlyCapture2::Image> &image) = 0;

  virtual bool ReadBuffer(std::unique_ptr<FlyCapture2::Image> &image) = 0;

  virtual bool StartLiveView() = 0;

  virtual bool StartCapture() = 0;

  virtual bool StopCapture() = 0;

  virtual bool ReadCameraInfo(std::unique_ptr<FlyCapture2::CameraInfo> &camera_info) = 0;

  virtual bool SetVideoModeAndFrameRate(const FlyCapture2::VideoMode video_mode, 
                                        const FlyCapture2::FrameRate frame_rate) = 0;

  virtual bool SetFC2Config(const std::unique_ptr<FlyCapture2::FC2Config> &config) = 0;

  virtual bool GetFC2Config(std::unique_ptr<FlyCapture2::FC2Config> &config) = 0;

  virtual bool GetTrigger(bool &trigger) = 0;

  virtual bool SetTrigger(const bool trigger_status) = 0;

  virtual bool FireSoftwareTrigger() = 0;

  virtual int GetBufferSize() = 0;

  /* \brief Get the serial number of the connected camera. 
   *
   * This is an ease of access function to get the serial 
   * number of the connected camera. 
   * @return if the operation succeded. 
   */
  virtual unsigned int GetSerialNumber() = 0;

  /* \brief Get the model name of the connected camera. 
   * 
   * This is an ease of access function to get the model 
   * name of the connected camera. 
   * @return if the operation succeded. 
   */
  virtual std::string GetModel() = 0;

  virtual bool IsConnected() = 0;

  virtual bool ReadProperty(std::unique_ptr<FlyCapture2::Property> &property, 
                            FlyCapture2::PropertyType type) = 0;

  virtual bool ReadPropertyInfo(std::unique_ptr<FlyCapture2::PropertyInfo> &property_info, 
                                FlyCapture2::PropertyType type) = 0;

  virtual bool SetProperty(const std::unique_ptr<FlyCapture2::Property> &property) = 0;

  virtual bool CheckVideoModeAndFrameRate(FlyCapture2::VideoMode video_mode, 
                                          FlyCapture2::FrameRate frame_rate) = 0;

}; // CameraCon

#endif // CAMERA_CON_H
