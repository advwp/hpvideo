#include "camera_scanner.h"

#include "camera_con.h"
#include "camera_con_local.h"
#include "camera_con_remote.h"

using std::vector;
using std::unique_ptr;
using std::move;

void CameraScanner::ScanLocalCameras(vector<unique_ptr<CameraConLocal>> &cameras) {

  FlyCapture2::Error error;
  FlyCapture2::BusManager bus_manager;

  unsigned int num_cameras;

  error = bus_manager.GetNumOfCameras(&num_cameras);
  
  if (error != FlyCapture2::PGRERROR_OK) {
    return;
  }

  for (unsigned int i = 0; i < num_cameras; i++) {
    FlyCapture2::PGRGuid guid;
    error = bus_manager.GetCameraFromIndex(i, &guid);

    if (error == FlyCapture2::PGRERROR_OK) {
      unique_ptr<CameraConLocal> temp_camera(new CameraConLocal(guid));
      cameras.push_back(move(temp_camera));
    }
  }  
}

void CameraScanner::ScanRemoteCameras(vector<unique_ptr<CameraConRemote>> &cameras) {
  // Not implemented.
}

void CameraScanner::GetAllCameras(vector<unique_ptr<CameraCon>> &cameras) {

  vector<unique_ptr<CameraConLocal>> local_cameras;
  ScanLocalCameras(local_cameras);
  
  vector<unique_ptr<CameraConRemote>> remote_cameras;
  ScanRemoteCameras(remote_cameras);

  for (auto &it : local_cameras) {
    auto tmp = dynamic_cast<CameraCon*>(it.get());
    unique_ptr<CameraCon> tmp_ptr(tmp);
    it.release();
    cameras.push_back(move(tmp_ptr));
  }
  for (auto &it : remote_cameras) {
    auto tmp = dynamic_cast<CameraCon*>(it.get());
    unique_ptr<CameraCon> tmp_ptr(tmp);
    it.release();
    cameras.push_back(move(tmp_ptr));
  }
}
