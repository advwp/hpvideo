#ifndef CAMERA_CON_REMOTE_H
#define CAMERA_CON_REMOTE_H

#include "camera_con.h"

/*! \brief Class controlling remote cameras. 
* 
* This class is not implemented. 
*/
class CameraConRemote : public CameraCon {

public:

  CameraConRemote(FlyCapture2::PGRGuid guid);

  virtual CAM_INTERFACE_TYPE GetType() { return REMOTE; }

  virtual bool GetNewImage(std::unique_ptr<FlyCapture2::Image> &image);
  virtual bool ReadBuffer(std::unique_ptr<FlyCapture2::Image> &image);

  virtual bool StartLiveView();
  virtual bool StartCapture();
  virtual bool StopCapture();

  virtual bool ReadCameraInfo(std::unique_ptr<FlyCapture2::CameraInfo> &camera_info);

  virtual bool SetVideoModeAndFrameRate(const FlyCapture2::VideoMode video_mode, 
                                        const FlyCapture2::FrameRate frame_rate);

  virtual bool SetFC2Config(const std::unique_ptr<FlyCapture2::FC2Config> &config);

  virtual bool GetFC2Config(std::unique_ptr<FlyCapture2::FC2Config> &config);

  virtual bool GetTrigger(bool &trigger);
  virtual bool SetTrigger(const bool trigger_status);
  virtual bool FireSoftwareTrigger();

  virtual int GetBufferSize();

  virtual unsigned int GetSerialNumber();

  virtual std::string GetModel();

  virtual bool IsConnected();

  virtual bool ReadProperty(std::unique_ptr<FlyCapture2::Property> &property, 
                            FlyCapture2::PropertyType type);

  virtual bool ReadPropertyInfo(std::unique_ptr<FlyCapture2::PropertyInfo> &property_info, 
                                FlyCapture2::PropertyType type);

  virtual bool SetProperty(const std::unique_ptr<FlyCapture2::Property> &property, 
                           FlyCapture2::PropertyType type);

  virtual bool CheckVideoModeAndFrameRate(FlyCapture2::VideoMode video_mode, 
                                          FlyCapture2::FrameRate frame_rate);

}; // CameraConRemote

#endif // CAMERA_CON_REMOTE_H
