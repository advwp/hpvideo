#include <QtGui>

#include "live_view_window.h"

#include "controller.h"

#include "logger.h"

#include <string>

LiveViewWindow::LiveViewWindow(std::shared_ptr<Controller> controller, 
                               const unsigned int serial_number, 
                               const QSize size, 
                               const QPoint pos, 
                               QWidget* /* UNUSED */) {

  setupUi(this);
  this->setAttribute( Qt::WA_DeleteOnClose );
  started_ = false;

  controller_ = controller;

  serial_number_ = serial_number;

  this->resize(size);
  this->move(pos);

  Init();
}

LiveViewWindow::~LiveViewWindow() {
  this->Stop();
}

void LiveViewWindow::Init() {

  timer_ = new QTimer(this);
  QGraphicsScene *scene = new QGraphicsScene(graphics_view);
  graphics_view->setScene(scene);

  ConnectSignals();

  QString title;
  title.setNum(serial_number_, 10);
  this->setWindowTitle(title);

  Start();
  this->move(QApplication::desktop()->screen()->rect().center() - this->rect().center());
}

void LiveViewWindow::ConnectSignals() {
  connect(timer_, SIGNAL(timeout()), this, SLOT(UpdateImage()));
}

void LiveViewWindow::Start() {
  if (controller_->StartLiveView(serial_number_)) {
    started_ = true;
    timer_->start(40);
  }
}

void LiveViewWindow::Stop() {
  timer_->stop();
  started_ = false;
  controller_->StopLiveView(serial_number_);
}

void LiveViewWindow::UpdateImage() {
  if (!started_) {
    Logger::Log(CAM_INFO, "not started");
    return;
  }

  unsigned int image_data_size = 0;
  std::unique_ptr<unsigned char> image_data;
  unsigned int image_width = 0;
  unsigned int image_height = 0;

  if (!controller_->GetImageData(serial_number_, image_data, image_data_size, image_width, image_height)) {
    Logger::Log(CAM_INFO, "failed getting image data.");
    return;
  }

  if (image_data == nullptr) {
    return;
  }

  QImage image(image_data.get(), image_width, image_height, image_width, QImage::Format_Indexed8);

  QImage scaled_image = image.scaled(this->width(), this->height(), Qt::KeepAspectRatio, Qt::FastTransformation);

  graphics_view->scene()->clear();
  QGraphicsPixmapItem *item = new QGraphicsPixmapItem( QPixmap::fromImage(scaled_image));
  graphics_view->scene()->addItem(item);
}
