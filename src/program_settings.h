#ifndef PROGRAM_SETTINGS_H
#define PROGRAM_SETTINGS_H

#include "ui_program_settings.h"


class ProgramSettings : public QWidget, private Ui::program_settings {

  Q_OBJECT

public: 

  /*! \brief Constructor
   * 
   * @param controller
   * @param camera_vector selected cameras. 
   * @param parent parent widget
   */
  ProgramSettings(QWidget *parent = 0);

public slots:

  /*! \brief Called when log info checkbox is clicked.
   */
  void CheckLogInfoClicked(bool state);

  /*! \brief Called when log critical checkbox is clicked.
   */
  void CheckLogCritClicked(bool state);

  /*! \brief Called when clear database button is clicked.
   */
  void BtnClearDatabaseClicked();

private: 

  /*! \brief Initial setup
   */
  void Init();

  /*! \brief Connect signals
   */
  void ConnectSignals();


};

#endif
