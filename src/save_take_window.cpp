#include <QtGui>

#include "save_take_window.h"
#include "controller.h"
#include "database.h"
#include "static_info.h"

#include <vector>

using std::shared_ptr;
using std::vector;

SaveTakeWindow::SaveTakeWindow(shared_ptr<Controller> controller, 
                               QWidget* /* UNUSED */) {
  setupUi(this);

  this->controller_ = controller;

  Init();
}

void SaveTakeWindow::ConnectSignals() {
  connect(btn_browse_files, SIGNAL( clicked() ), this, SLOT( BtnBrowseFilesClicked() ));
  connect(btn_save, SIGNAL( clicked() ), this, SLOT( BtnSaveClicked() ));
  connect(btn_cancel, SIGNAL( clicked() ), this, SLOT( BtnCancelClicked() ));
  connect(radio_avi, SIGNAL( clicked() ), this, SLOT( RadioAviClicked() ));
  connect(radio_raw, SIGNAL( clicked() ), this, SLOT( RadioRawClicked() ));
}

void SaveTakeWindow::Init() {

  tree_camera_list->setColumnCount(2);
  QStringList lbl_camera_list;
  lbl_camera_list << "Name" << "Serial";
  tree_camera_list->setHeaderLabels(lbl_camera_list);

  lbl_status->setText("Idle");

  //progress_save->setMinimum(0);
  //progress_save->setMaximum(100);
  //progress_save->setValue(0);

  radio_avi->setChecked(true);
  filter_ = kFilterAvi;

  take_id_ = -1;
  fps_ = -1.0;

  ConnectSignals();
}

void SaveTakeWindow::SetSelection(const int take_id, const float fps) {
  this->take_id_ = take_id;
  this->fps_ = fps;
}

void SaveTakeWindow::BtnBrowseFilesClicked() {

  QString caption = "Save As";
  QString current_directory = QDir::current().path();
  QString file_name = QFileDialog::getSaveFileName(this, caption, current_directory, filter_);

  line_save_location->insert(file_name);
}

void SaveTakeWindow::BtnSaveClicked() {

  if ((take_id_ > 0) && (fps_ > 0) && (GetSelectedCamera() > 0)) {

    if (filter_.compare(kFilterAvi) == 0) {
      lbl_status->setText("Saving to AVI");                        
      controller_->WriteImagesToAvi(GetSelectedCamera(), take_id_, fps_);

    } else if (filter_.compare(kFilterRaw) == 0) {
      lbl_status->setText("Saving to raw data");                        
      // TODO: Filewriter to raw data.

    }
  }
  lbl_status->setText("Idle");
}

void SaveTakeWindow::BtnCancelClicked() {
  this->close();
}

void SaveTakeWindow::RadioAviClicked() {
  filter_ = kFilterAvi;
}

void SaveTakeWindow::RadioRawClicked() {
  filter_ = kFilterRaw;
}

unsigned int SaveTakeWindow::GetSelectedCamera() {
  
  QList<QTreeWidgetItem *> list = tree_camera_list->selectedItems();

  if (list.size() <= 0) {
    return -1;
  }

  bool ok = false;
  unsigned int ret = list.at(0)->text(1).toUInt(&ok, 10);

  if (ok) {
    return ret;
  } else {
    return -1;
  }
}

void SaveTakeWindow::UpdateCameraList() {

  Database database(DB_NAME);
  tree_camera_list->clear();

  if (take_id_ < 0) {
    return; 
  }

  vector<CameraStruct> cameras;

  database.GetCameraListFromTake(take_id_, cameras);

  for (auto current_camera : cameras) {
    QString q_str_name(current_camera.name.c_str());
    QString q_str_serial_number;
    q_str_serial_number.setNum(current_camera.serial_number, 10);
    AddCameraToList(q_str_name, q_str_serial_number);
  }
}

void SaveTakeWindow::AddCameraToList(const QString name, const QString serial) {
  QTreeWidgetItem *item = new QTreeWidgetItem(tree_camera_list);

  item->setText(0, name);
  item->setText(1, serial);

  tree_camera_list->addTopLevelItem(item);
}
