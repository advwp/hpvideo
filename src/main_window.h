#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "ui_main_window.h"

#include <memory>
#include <vector>

class Controller;

/*! \brief Class controlling the main window. 
*/
class MainWindow : public QWidget, private Ui::win_main {

  Q_OBJECT

public: 

  /*! \brief Constructor
   *
   * @param parent this is not used. Top window. 
   */
  MainWindow(QWidget *parent = 0);

  /*! \brief Sets the name in the camera list. 
   *
   * Used when name gets updated. 
   * @param serial_number
   * @param name
   */
  void SetCameraName(const unsigned int serial_number, const QString &name);

public slots: 

  /*! \brief Called when the refresh button is clicked. 
  */
  void BtnRefreshClicked();

  /*! \brief Called when the configure button is clicked. 
  */
  void BtnConfigureSelectedClicked();

  /*! \brief Called when the live view button is clicked. 
  */
  void BtnLiveViewClicked();

  /*! \brief Called when the record button is clicked. 
  */
  void BtnRecordClicked();

  /*! \brief Called when the takes button is clicked. 
  */
  void BtnTakesClicked();

  /*! \brief Called when the takes button is clicked. 
  */
  void BtnProgramSettingsClicked();

private:

  /*! \brief Initial setup. 
  */
  void Init();

  /*! \brief Connect signals. 
  */
  void ConnectSignals();

  /*! \brief Add camera to the list. 
   * 
   * This adds a list entry of the given camera. 
   * @param name
   * @param serial_number 
   * @param model 
   * @param interface
   * @param ip
   */
  void AddCameraToList(QString name, QString serial_number, QString model, QString interface, QString ip);

  /*! \brief Get the items currently selected. 
  */
  std::vector<unsigned int> GetSelections();
  
  /*! \brief Get new position of live view window. 
   *
   * Live view windows are positioned beside one another. 
   * This function decides where on the screen the next window 
   * should spawn.
   * @param size
   * @param pos
   */
  void GetNextSizeAndPos(QSize &size, QPoint &pos);

  std::shared_ptr<Controller> controller_; //!< Controller

};

#endif
