#ifndef DATABASE_H
#define DATABASE_H

#include <string>
#include <map>
#include <memory>
#include <vector>
#include <sqlite3.h>

#include "static_info.h"

#include "FlyCapture2.h"

class CameraConLocal;

/*! \brief Database handler class. 
* 
* This class handles all database transactions. 
* 
* Note that it is not dependant on any of the other classes 
* in the application (except for FlyCapture2 specific classes) 
* so that it can be reimplemented without changing other files. 
*/
class Database {

public: 
  /* \brief Constructor
   * 
   * The constructor connects to the given name. 
   * Use connected_status() to check if the constructor
   * succeded. 
   */
  Database(std::string database_name);

  /* \brief Destructor
   * 
   * The destructor closes the database connection. 
   * This makes sure there are never any unclosed connections 
   * to the database. 
   */
  ~Database();

  /* \brief Check if the constructor succeded. 
   * 
   * @return if the constructor succeded. 
   */
  bool connected_status() { return connected_status_; }

  /* \brief Save a camera in the database. 
   * 
   * This is used to store information on a single camera. 
   * Used by UpdateCameras().
   * @param serial_number
   * @param name
   * @param model_name 
   * @param vendor_name 
   * @return status code. 
   */
  DB_STATUS SaveCamera(const unsigned int serial_number, 
                       const std::string name, 
                       const std::string model_name, 
                       const std::string vendor_name);

  /* \brief Get the name corresponding to the given serial number. 
   * 
   * @param serial_number
   * @param *OUT* name 
   * @return status code. 
   */
  DB_STATUS GetCameraName(const unsigned int serial_number, 
                          std::string &name);

  /* \brief Save a name an serial number connection. 
   * 
   * @param serial_number
   * @param name
   * @return status code. 
   */
  DB_STATUS SetCameraName(const unsigned int serial_number, 
                          const std::string &name);

  /* \brief Store a single image in the database. 
   * 
   * @param image image to be stored. 
   * @param serial_number the camera that captured the image. 
   * @param take_id id of the take.
   * @return status code.
   */
  DB_STATUS SaveImage(const std::unique_ptr<FlyCapture2::Image> &image, 
                      const int serial_number, 
                      const int take_id);
  
  /* \brief Get images from a specific take and camera. 
   * 
   * @param serial_number
   * @param take_id 
   * @param *OUT* images
   * @return status code. 
   */
  DB_STATUS GetImages(const unsigned int serial_number, 
                      const int take_id, 
                      std::vector<std::unique_ptr<FlyCapture2::Image>> &images);

  /* \brief Update cameras stored in the database. 
   * 
   * Save any unsaved cameras in the database. 
   * @param all_cameras map of cameras to add. 
   * @return status code. 
   */
  DB_STATUS UpdateCameras(std::map<unsigned int, std::shared_ptr<CameraConLocal>> &all_cameras);

  /* \brief Get a vector of all takes. 
   * 
   * Get a vector containing information on all previously 
   * stored takes. 
   * @param takes 
   * @return status code. 
   */
  DB_STATUS GetTakes(std::vector<TakeStruct> &takes);

  /* \brief Set a new take start. 
   *
   * This sets the date and time of when the take was started. 
   * @param fps frames per second used in the take. 
   * @param *OUT* take_id id of the started take. 
   * @return status code. 
   */
  DB_STATUS StartNewTake(const double fps, int &take_id);

  /* \brief End take. 
   * 
   * This function is nessecary to set the end date and time of the take. 
   * @param take_id
   * @return status code.
   */
  DB_STATUS EndTake(const int take_id);

  /* \brief Get the latest take id. 
   * 
   * Get the id of the take last made. 
   * @param *OUT* take_id
   * @return status code.
   */
  DB_STATUS GetLatestTakeId(int &take_id);

  /* \brief Get cameras used in a take. 
   * 
   * @param take_id 
   * @param *OUT* cameras store camera information here. 
   * @return status code. 
   */
  DB_STATUS GetCameraListFromTake(const int take_id, 
                                  std::vector<CameraStruct> &cameras);

  /* \brief Delete a take. 
   * 
   * This deletes the take information as well as 
   * all frames stored with the given take id. 
   * @param take_id
   * @return status code. 
   */
  DB_STATUS DeleteTake(const int take_id);

  /* \brief Clears the database. 
   * 
   * This deletes ALL information stored in the database. 
   */
  void ClearDatabase();
  
private: 
  sqlite3 *database; //!< Database. 

  std::string database_name_; //!< Name of the database file. 

  bool connected_status_; //!< Status of constructor. 

}; // Database

#endif // DATABASE_H
