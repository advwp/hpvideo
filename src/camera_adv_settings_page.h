#ifndef CAMERA_ADV_SETTINGS_PAGE_H
#define CAMERA_ADV_SETTINGS_PAGE_H

#include "ui_camera_adv_settings_page.h"

#include "FlyCapture2.h"

#include <memory>
#include <vector>

class Controller;

/*! \brief Class defining the advanced settings page. 
*/
class CameraAdvSettingsPage : public QWidget, private Ui::camera_adv_settings_page {

  Q_OBJECT

public:
  
  /*! \brief Constructor
   * 
   * @param controller
   * @param camera_vector selected cameras. 
   * @param parent parent widget
   */
  CameraAdvSettingsPage(std::shared_ptr<Controller> controller, std::vector<unsigned int> &camera_vector, QWidget *parent = 0);

public slots:

  /*! \brief Called when internal buffers spinbox is changed.
   */
  void InternalBuffersUpdated(int int_buf);

  /*! \brief Called when image notifications spinbox is changed. 
   */
  void ImageNotificationsUpdated(int img_not);

  /*! \brief Called when grab timeout spinbox is changed.
   */
  void GrabTimeoutUpdated(int g_time);

private: 

  /*! \brief Initial setup
   */
  void Init();

  /*! \brief Connect signals
   */
  void ConnectSignals();

  std::shared_ptr<Controller> controller_; //!< Controller 
  std::vector<unsigned int> camera_vector_; //!< Selected cameras

};

#endif
