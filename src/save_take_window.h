#ifndef SAVE_TAKE_WINDOW_H
#define SAVE_TAKE_WINDOW_H

#include <memory>

#include "ui_save_take_window.h"

class Controller;

/*! \brief Class controlling the save take window
*/
class SaveTakeWindow : public QWidget, private Ui::win_save_take {

  Q_OBJECT

public:

  /*! \brief Constructor
   * 
   * @param controller
   * @param parent parent widget
   */
  SaveTakeWindow(std::shared_ptr<Controller> controller, 
                 QWidget *parent = 0);

  /*! \brief Set local variables. 
  */
  void SetSelection(const int takeId, const float fps);
  void SetFps(double fps) { fps_ = fps; }

  /*! \brief Update cameras used in take from the database. 
  */
  void UpdateCameraList();

public slots:

  /*! \brief Called when browse button is clicked. 
  */
  void BtnBrowseFilesClicked();

  /*! \brief Called when save button is clicked. 
  */
  void BtnSaveClicked();

  /*! \brief Called when cancel button is clicked. 
  */
  void BtnCancelClicked();

  /*! \brief Called when avi button is clicked. 
  */
  void RadioAviClicked();

  /*! \brief Called when raw button is clicked. 
  */
  void RadioRawClicked();


private:
  /*! \brief Connect signals
  */
  void ConnectSignals();
  
  /*! \brief Initial setup
  */
  void Init();

  /*! \brief Adds a camera to the camera list. 
  */
  void AddCameraToList(const QString name, const QString serial);

  /*! \brief Get the serial number of the selected camera. 
  */
  unsigned int GetSelectedCamera();

  QString filter_; //!< Used to set the file type filter in the browse dialog. 

  int take_id_; //!< Take id 
  double fps_; //!< Frames per second

  std::shared_ptr<Controller> controller_; //!< Controller

};

const QString kFilterAvi = "AVI files (*.avi)"; //!< Filter option for AVI
const QString kFilterRaw = "Take files (*.tak)"; //!< Filter option for RAW

#endif
