######################################################################
# Qt project file for Vizlab video application. 
######################################################################

TEMPLATE = app
TARGET = video
DEPENDPATH += .

win32:INCLUDEPATH += "C:/Program Files/Point Grey Research/FlyCapture2/include"
win32:INCLUDEPATH += "C:/Sqlite"
unix:INCLUDEPATH += "/usr/include/flycapture"

unix:LIBS += -lflycapture${D} \ 
             -lsqlite3

win32:LIBS += "c:/Sqlite/sqlite3.lib" \
              -L"c:/Program Files/Point Grey Research/FlyCapture2/lib" \
              -l"c:/Program Files/Point Grey Research/FlyCapture2/lib/FlyCapture2"

unix:QMAKE_CXXFLAGS += -std=c++0x -g 

unix:DESTDIR = ../bin/

# Input
HEADERS += camera.h \
           camera_con.h \
           camera_con_local.h \
           camera_con_remote.h \
           camera_scanner.h \
           camera_settings_page.h \
           camera_info_page.h \
           camera_adv_settings_page.h \
           video_modes_page.h \
           camera_settings_window.h \
           controller.h \
           database.h \
           file_writer.h \
           global_settings.h \
           live_view_window.h \
           logger.h \
           main_window.h \
           program_settings.h \
           record_window.h \
           save_take_window.h \
           static_info.h \
           takes_window.h

FORMS += ui/camera_config_window.ui \
         ui/camera_settings_page.ui \
         ui/camera_settings_window.ui \
         ui/camera_info_page.ui \
         ui/camera_adv_settings_page.ui \
         ui/video_modes_page.ui \
         ui/live_view_window.ui \
         ui/program_settings.ui \
         ui/main_window.ui \
         ui/record_window.ui \
         ui/save_take_window.ui \
         ui/takes_window.ui

SOURCES += camera.cpp \
           camera_con_local.cpp \
           camera_scanner.cpp \
           camera_settings_page.cpp \
           camera_info_page.cpp \
           camera_adv_settings_page.cpp \
           video_modes_page.cpp \
           camera_settings_window.cpp \
           controller.cpp \
           database.cpp \
           file_writer.cpp \
           global_settings.cpp \
           live_view_window.cpp \
           program_settings.cpp \
           logger.cpp \
           main_window.cpp \
           record_window.cpp \
           save_take_window.cpp \
           takes_window.cpp \
           main.cpp
