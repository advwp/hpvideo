#include "camera.h"
#include "static_info.h"
#include "logger.h"
#include "global_settings.h"

#include <iostream>

Camera::Camera(FlyCapture2::PGRGuid guid) {

  FlyCapture2::Error error;

  camera_ = std::make_shared<FlyCapture2::Camera>();
  image_buffer_ = std::make_shared<std::list<std::unique_ptr<FlyCapture2::Image>>>();
  image_buffer_mutex_ = std::make_shared<std::mutex>();
  capture_mutex_ = std::make_shared<std::mutex>();
  capture_in_progress_ = std::make_shared<bool>(false);

  *capture_in_progress_ = false;

  error = camera_->Connect(&guid);

  if (error != FlyCapture2::PGRERROR_OK) {  // Camera connection failed. 
    Logger::Log(CAM_CRIT, "Error connecting to camera.");;
    connected_ = false;

  } else {  // Camera connection succesful. 

    connected_ = true;

    // Set some default camera options.
    FlyCapture2::FC2Config pConfig;
    camera_->GetConfiguration(&pConfig);
    pConfig.grabMode = FlyCapture2::BUFFER_FRAMES;
    pConfig.numBuffers = 10;
    pConfig.bandwidthAllocation = FlyCapture2::BANDWIDTH_ALLOCATION_ON;
    pConfig.grabTimeout = 0;
    pConfig.numImageNotifications = 2;
    camera_->SetConfiguration(&pConfig);

    // Set local serial number variable.
    FlyCapture2::CameraInfo cam_info;
    error = camera_->GetCameraInfo(&cam_info);
    if (error != FlyCapture2::PGRERROR_OK) {
      Logger::Log(CAM_CRIT, "Failed getting camera info in constructor.");
      serial_number_ = 0;
    } else {
      serial_number_ = cam_info.serialNumber;
    }
  }

}

Camera::~Camera() {
  camera_->StopCapture();
  camera_->Disconnect();
}

bool Camera::StartLiveView() {

  FlyCapture2::FC2Config pConfig;
  camera_->GetConfiguration(&pConfig);
  pConfig.grabMode = FlyCapture2::DROP_FRAMES;
  camera_->SetConfiguration(&pConfig);

  this->SetTrigger(false);

  FlyCapture2::Error error = camera_->StartCapture();
  if (error != FlyCapture2::PGRERROR_OK) {
    Logger::Log(CAM_CRIT, serial_number_, "Error starting live view.");
    return false;

  }

  return true;
}

bool Camera::StartCapture() {

  FlyCapture2::FC2Config pConfig;
  camera_->GetConfiguration(&pConfig);
  pConfig.grabMode = FlyCapture2::BUFFER_FRAMES;
  camera_->SetConfiguration(&pConfig);

  this->SetTrigger(true);

  FlyCapture2::Error error = camera_->StartCapture();
  if (error != FlyCapture2::PGRERROR_OK) {
    Logger::Log(CAM_CRIT, serial_number_, "Error starting capture.");
    *capture_in_progress_ = false;

    return false;
  }

  capture_mutex_->lock();
  *capture_in_progress_ = true;
  capture_mutex_->unlock();

  // Start the capture thread if the camera successfully started capturing. 
  threads_.push_back(std::thread(CaptureImages, 
                                 camera_, 
                                 image_buffer_, 
                                 capture_in_progress_, 
                                 capture_mutex_, 
                                 image_buffer_mutex_));

  return true;
}

bool Camera::StopCapture() {

  // Set capture_in_progress to false and wait for the capture thread to join.
  capture_mutex_->lock();
  *capture_in_progress_ = false;
  capture_mutex_->unlock();

  for (auto &t : threads_) {
    t.detach();
  }
  // Delete the thread from the thread container. 
  threads_.clear();

  // Stop the capture. 
  FlyCapture2::Error error = camera_->StopCapture();
  if (error != FlyCapture2::PGRERROR_OK) {

    if (error == FlyCapture2::PGRERROR_ISOCH_NOT_STARTED) {
      Logger::Log(CAM_INFO, serial_number_, "Camera capture not started.");
    } else {
      Logger::Log(CAM_INFO, serial_number_, "Camera capture stop failed.");
    }

    return false;
  }

  return true;
}

bool Camera::SetVideoModeAndFrameRate(const FlyCapture2::VideoMode video_mode, 
                              const FlyCapture2::FrameRate frame_rate) {
  if (camera_->SetVideoModeAndFrameRate(video_mode, frame_rate) == FlyCapture2::PGRERROR_OK) {
    return true;
  }
  return false;
}

bool Camera::SetFC2Config(const std::unique_ptr<FlyCapture2::FC2Config> &config) {

  if (camera_->SetConfiguration(config.get()) != FlyCapture2::PGRERROR_OK) {
    Logger::Log(CAM_CRIT, serial_number_, "Unable to set FC2Config.");
    return false;
  }
  return true;
}

bool Camera::GetFC2Config(std::unique_ptr<FlyCapture2::FC2Config> &config) {

  FlyCapture2::Error error;

  std::unique_ptr<FlyCapture2::FC2Config> tmp_config(new FlyCapture2::FC2Config());

  error = camera_->GetConfiguration(tmp_config.get());

  if (error != FlyCapture2::PGRERROR_OK) {
    config = nullptr;
    Logger::Log(CAM_CRIT, serial_number_, "Unable to get FC2Config.");
    return false;
  }

  config = std::move(tmp_config);

  return true;
}

bool Camera::ReadCameraInfo(std::unique_ptr<FlyCapture2::CameraInfo> &camInfo) {

  FlyCapture2::Error error;

  std::unique_ptr<FlyCapture2::CameraInfo> tmpCamInfo(new FlyCapture2::CameraInfo());

  error = camera_->GetCameraInfo(tmpCamInfo.get());

  if (error != FlyCapture2::PGRERROR_OK) {
    camInfo = nullptr;
    Logger::Log(CAM_CRIT, serial_number_, "Could not retrieve camera info.");
    return false;
  }

  if (tmpCamInfo == nullptr) {
    Logger::Log(CAM_CRIT, serial_number_, "Could not retrieve camera info2.");
    return false;
  }

  if (tmpCamInfo->modelName == nullptr) {
    Logger::Log(CAM_CRIT, serial_number_, "Could not retrieve camera info2.");
    return false;
  }

  camInfo = std::move(tmpCamInfo);

  return true;
}

bool Camera::ReadProperty(std::unique_ptr<FlyCapture2::Property> &property, 
                          FlyCapture2::PropertyType type) {

  std::unique_ptr<FlyCapture2::Property> camera_property(new FlyCapture2::Property());
  camera_property->type = type;

  FlyCapture2::Error error = camera_->GetProperty(camera_property.get());
  if (error != FlyCapture2::PGRERROR_OK) {
    property = nullptr;
    Logger::Log(CAM_CRIT, serial_number_, "Could not retrieve property.");;
    return false;
  }

  property = std::move(camera_property);

  return true;
}

bool Camera::ReadPropertyInfo(std::unique_ptr<FlyCapture2::PropertyInfo> &property_info, 
                              FlyCapture2::PropertyType type) {

  std::unique_ptr<FlyCapture2::PropertyInfo> camera_property_info(new FlyCapture2::PropertyInfo());
  camera_property_info->type = type;

  FlyCapture2::Error error = camera_->GetPropertyInfo(camera_property_info.get());
  if (error != FlyCapture2::PGRERROR_OK) {
    property_info = nullptr;
    Logger::Log(CAM_CRIT, serial_number_, "Could not retrieve property information.");
    return false;
  }

  property_info = std::move(camera_property_info);

  return true;
}

bool Camera::SetProperty(const std::unique_ptr<FlyCapture2::Property> &property) {

  FlyCapture2::Error error = camera_->SetProperty(property.get());
  if (error != FlyCapture2::PGRERROR_OK) {
    Logger::Log(CAM_CRIT, serial_number_, "Failed setting property.");
    return false;
  }

  return true;
}

bool Camera::ReadBuffer(std::unique_ptr<FlyCapture2::Image> &image) {

  if (!image_buffer_->empty()) {
    image = std::move(image_buffer_->front());
    
    image_buffer_mutex_->lock();
    image_buffer_->pop_front();
    image_buffer_mutex_->unlock();

    Logger::Log(CAM_INFO, serial_number_, "Read image from camera buffer. ");

    return true;
  }

  return false;
}

bool Camera::SetTrigger(const bool trigger_status) {

  FlyCapture2::TriggerMode trigger_mode;

  FlyCapture2::Error error = camera_->GetTriggerMode(&trigger_mode);
  if (error != FlyCapture2::PGRERROR_OK) {
    Logger::Log(CAM_CRIT, serial_number_, "Error getting trigger mode from camera.");
    return false;
  }

  if (trigger_mode.onOff != trigger_status) {
    trigger_mode.onOff = !trigger_mode.onOff;
  }

  error = camera_->SetTriggerMode(&trigger_mode, false);
  if (error != FlyCapture2::PGRERROR_OK) {
    Logger::Log(CAM_CRIT, serial_number_, "Error setting trigger mode on camera.");
    return false;        
  }

  return true;
}

bool Camera::GetTrigger(bool &trigger) {

  FlyCapture2::TriggerMode trigger_mode;

  FlyCapture2::Error error = camera_->GetTriggerMode(&trigger_mode);
  if (error != FlyCapture2::PGRERROR_OK) {
    Logger::Log(CAM_CRIT, serial_number_, "Error getting trigger mode from camera.");
    return false;
  }

  trigger = trigger_mode.onOff;

  return true;
}

bool Camera::FireSoftwareTrigger() {
  FlyCapture2::Error error;

  error = camera_->FireSoftwareTrigger();

  return error == FlyCapture2::PGRERROR_OK ? true : false;
}

bool Camera::GetNewImage(std::unique_ptr<FlyCapture2::Image> &image) {

  std::unique_ptr<FlyCapture2::Image> tmp_image(new FlyCapture2::Image());
  FlyCapture2::Error error; 
  
  error = camera_->RetrieveBuffer(tmp_image.get());

  if (error != FlyCapture2::PGRERROR_OK) {
    Logger::Log(CAM_CRIT, serial_number_, "Image retrieval failed.");
    return false;
  }
  if (tmp_image != nullptr && tmp_image->GetDataSize() > 0) {
    image = std::move(tmp_image);
    return true;
  }

  return false;
}

void Camera::CaptureImages(const std::shared_ptr<FlyCapture2::Camera> camera, 
                           const std::shared_ptr<std::list<std::unique_ptr<FlyCapture2::Image>>> image_buffer, 
                           const std::shared_ptr<bool> capture_in_progress,
                           const std::shared_ptr<std::mutex> capture_mutex, 
                           const std::shared_ptr<std::mutex> image_buffer_mutex) {

  FlyCapture2::Error error;

  // Keep on capturing until the capture_in_progress variable is false, 
  // or until the image buffer is full. 
  while (image_buffer->size() < kMaxBufferSize) {

    capture_mutex->lock();
    if (!*capture_in_progress) {
      break;
    } 
    capture_mutex->unlock();

    std::unique_ptr<FlyCapture2::Image> raw_image(new FlyCapture2::Image());
    error = camera->RetrieveBuffer(raw_image.get());
    
    // Continue if the image was not successfully retrieved. 
    if (error != FlyCapture2::PGRERROR_OK) {
      Logger::Log(CAM_CRIT, "Could not retrieve image from camera.");
      continue;
    }

    image_buffer_mutex->lock();
    image_buffer->push_back(std::move(raw_image));
    image_buffer_mutex->unlock();
  }

}

bool Camera::CheckVideoModeAndFrameRate(FlyCapture2::VideoMode video_mode, 
                                        FlyCapture2::FrameRate frame_rate) {

  FlyCapture2::Error error;

  bool supported = false;

  error = camera_->GetVideoModeAndFrameRateInfo(video_mode, frame_rate, &supported);

  if (error != FlyCapture2::PGRERROR_OK) {
    Logger::Log(CAM_CRIT, serial_number_, "Failed checking for video mode + frame rate support.");
    return false;
  }

  return supported;
}

bool Camera::ValidateTimeStamps(double fps, 
                                long long s1, 
                                unsigned int mic_s1, 
                                long long s2, 
                                unsigned int mic_s2) {

  double tmp = ((double)1 / fps);
  unsigned int max_micro_s = static_cast<unsigned int>(tmp * 1000 * 1000 * 1.5);
  if (s2 - s1 == 0 && (mic_s2 - mic_s1) < max_micro_s) {
    return true;
  } else if (s2 - s1 == 1 && (mic_s1 - mic_s2) < max_micro_s) {
    return true;
  } else {
    return false;
  }
}


