#include "database.h"

#include "logger.h"
#include "camera_con_local.h"

#ifndef WIN32
#include <sys/time.h>
#endif

#include <iostream>

using std::string;
using std::vector;
using std::unique_ptr;
using std::shared_ptr;
using std::map;
using std::move;

Database::Database(string database_name) {

  database_name_ = database_name;
  database = nullptr;

  int rc = sqlite3_open(database_name_.c_str(), &database);
  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Could not connect to database.");
    sqlite3_close(database);
    connected_status_ = false;
  } else {

    char *error_message;
    rc = sqlite3_exec(database, "PRAGMA synchronous = OFF", nullptr, nullptr, &error_message);

    if (rc != SQLITE_OK) {
      sqlite3_close(database);
      Logger::Log(DB_CRIT, "Database failed setting PRAGMA synchronous = OFF");
      connected_status_ = false;
    } else {
      connected_status_ = true;
    } // else

  } // else

}

Database::~Database() {
  sqlite3_close(database);
}

DB_STATUS Database::SaveCamera(const unsigned int serial_number, 
                               const string camera_name, 
                               const string model_name, 
                               const string vendor_name) {

  string sql_string = "INSERT INTO Cameras(serial_number, name, model_name, vendor_name) VALUES(?, ?, ?, ?)";

  sqlite3_stmt* prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement in SaveCamera()");
    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_int(prepared_statement, 1, serial_number) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveCamera()");
    sqlite3_finalize(prepared_statement);
    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_text(prepared_statement, 2, camera_name.c_str(), -1, SQLITE_STATIC) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveCamera()");
    sqlite3_finalize(prepared_statement);
    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_text(prepared_statement, 3, model_name.c_str(), -1, SQLITE_STATIC) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveCamera()");
    sqlite3_finalize(prepared_statement);
    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_text(prepared_statement, 4, vendor_name.c_str(), -1, SQLITE_STATIC) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveCamera()");
    sqlite3_finalize(prepared_statement);
    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);

  // Camera aldready saved. 
  if (rc == SQLITE_CONSTRAINT) {

    sqlite3_finalize(prepared_statement);

    return DB_CONSTRAINT_ERROR;

  } else if (rc != SQLITE_DONE) {

    Logger::Log(DB_CRIT, "Database failed finalizing prepared statement in SaveCamera()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  sqlite3_finalize(prepared_statement);

  return DB_OK;
}

DB_STATUS Database::GetCameraName(const unsigned int serial_number, string &name) {

  string sql_string = "SELECT name FROM Cameras WHERE serial_number = ?";

  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement in GetCameraName()");
    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_int(prepared_statement, 1, serial_number) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in GetCameraName()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);
  if (rc != SQLITE_ROW) {
    Logger::Log(DB_CRIT, "Database failed stepping in GetCameraName()");
    return DB_SQL_ERROR; 
  }

  const unsigned char *returned_name = sqlite3_column_text(prepared_statement, 0);

  if (sqlite3_finalize(prepared_statement) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed finalizing prepared statement in GetCameraName()");
  }

  const char *c_name = reinterpret_cast<const char*>(returned_name);
  if (c_name != nullptr) {
    string ret(c_name);
    name = ret;
  } else {
    name = "";
  }

  return DB_OK;
}

DB_STATUS Database::SetCameraName(const unsigned int serial_number, const string &name) {

  string sql_string = "UPDATE Cameras SET name=? WHERE serial_number = ?";
  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement int SetCameraName()");
    return DB_SQL_ERROR; 
  }

  if (sqlite3_bind_text(prepared_statement, 1, name.c_str(), -1, SQLITE_STATIC) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SetCameraName()");
    sqlite3_finalize(prepared_statement);
    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_int(prepared_statement, 2, serial_number) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SetCameraName()");
    sqlite3_finalize(prepared_statement);
    return DB_SQL_ERROR; 
  }

  rc = sqlite3_step(prepared_statement);
  if (rc != SQLITE_DONE) {
    Logger::Log(DB_CRIT, "Database failed stepping in SetCameraName()");
    sqlite3_finalize(prepared_statement);
    return DB_SQL_ERROR;
  }

  sqlite3_finalize(prepared_statement);

  return DB_OK;
}

DB_STATUS Database::SaveImage(const unique_ptr<FlyCapture2::Image> &image, 
                              const int serial_number, 
                              const int take_id) {

  string sql_string = "INSERT INTO Images";
  sql_string += "(camera_serial_number, take_id, rows, cols, stride, image_data, data_size, pixel_format, bayer_tile_format, time_seconds, time_micro_seconds) ";
  sql_string += "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement in SaveImage()");
    return DB_SQL_ERROR; 
  }

  if (sqlite3_bind_int(prepared_statement, 1, serial_number) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR; 
  }

  if (sqlite3_bind_int(prepared_statement, 2, take_id) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR; 
  }

  if (sqlite3_bind_int(prepared_statement, 3, image->GetRows()) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR; 
  }

  if (sqlite3_bind_int(prepared_statement, 4, image->GetCols()) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR; 
  }

  if (sqlite3_bind_int(prepared_statement, 5, image->GetStride()) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR; 
  }

  if (sqlite3_bind_blob(prepared_statement, 6, image->GetData(), image->GetDataSize(), SQLITE_STATIC) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;        
  }

  if (sqlite3_bind_int(prepared_statement, 7, image->GetDataSize()) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR; 
  }

  if (sqlite3_bind_int(prepared_statement, 8, (int)image->GetPixelFormat()) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR; 
  }

  if (sqlite3_bind_int(prepared_statement, 9, (int)image->GetBayerTileFormat()) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR; 
  }       

  FlyCapture2::TimeStamp time_stamp = image->GetTimeStamp();
  if (sqlite3_bind_int64(prepared_statement, 10, time_stamp.seconds) != SQLITE_OK) {
    sqlite3_finalize(prepared_statement);
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");

    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_int(prepared_statement, 11, time_stamp.microSeconds) != SQLITE_OK) {
    sqlite3_finalize(prepared_statement);
    Logger::Log(DB_CRIT, "Database failed binding variable in SaveImage()");

    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);
  if (rc != SQLITE_DONE) {
    sqlite3_finalize(prepared_statement);
    Logger::Log(DB_CRIT, "Database failed finalizing statement in SaveImage()");

    return DB_SQL_ERROR;
  }

  sqlite3_finalize(prepared_statement);


  return DB_OK;
}

DB_STATUS Database::GetImages(const unsigned int serial_number, 
                              const int take_nr, 
                              vector<unique_ptr<FlyCapture2::Image>> &images) {

  string sql_string = "SELECT rows, cols, stride, image_data, data_size, pixel_format, bayer_tile_format ";
  sql_string += "FROM Images WHERE camera_serial_number=? AND take_id=?";

  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statment in GetImages()");
    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_int(prepared_statement, 1, serial_number) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in GetImages()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_int(prepared_statement, 2, take_nr) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in GetImages()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }


  rc = sqlite3_step(prepared_statement);
  while (rc != SQLITE_DONE) {

    if (rc != SQLITE_ROW) {
      Logger::Log(DB_CRIT, "Database failed during stepping in GetImages()");
      sqlite3_finalize(prepared_statement);

      return DB_SQL_ERROR; 
    }

    unique_ptr<int> image_data_size(new int(0));
    unique_ptr<unsigned char*> image_data(new unsigned char*(0));

    unsigned int rows = sqlite3_column_int(prepared_statement, 0);
    unsigned int cols = sqlite3_column_int(prepared_statement, 1);
    unsigned int stride = sqlite3_column_int(prepared_statement, 2);
    *image_data_size = sqlite3_column_bytes(prepared_statement, 3);
    *image_data = (unsigned char *)malloc(*image_data_size);
    memcpy(*image_data, sqlite3_column_blob(prepared_statement, 3), *image_data_size);
    unsigned int data_size = sqlite3_column_int(prepared_statement, 4);
    FlyCapture2::PixelFormat pixel_format = (FlyCapture2::PixelFormat)sqlite3_column_int(prepared_statement, 5);
    FlyCapture2::BayerTileFormat bayer_tile_format = (FlyCapture2::BayerTileFormat)sqlite3_column_int(prepared_statement, 5);

    unique_ptr<FlyCapture2::Image> temp_image(new FlyCapture2::Image(rows, 
                                                                     cols, 
                                                                     stride, 
                                                                     *image_data, 
                                                                     data_size, 
                                                                     pixel_format, 
                                                                     bayer_tile_format));
    images.push_back(move(temp_image));

    rc = sqlite3_step(prepared_statement);

  } // while

  sqlite3_finalize(prepared_statement);

  return DB_OK;
}

DB_STATUS Database::UpdateCameras(map<unsigned int, shared_ptr<CameraConLocal>> &all_cameras) {

  for (auto &current_camera : all_cameras) {
    
    unique_ptr<FlyCapture2::CameraInfo> camera_info;

    if (!current_camera.second->ReadCameraInfo(camera_info)) {
      Logger::Log(DB_CRIT, "Failed to read camera info in Database::UpdateCameras()");
      return DB_ERROR;
    } else {
      string model_name(camera_info->modelName);
      string vendor_name(camera_info->vendorName);

      SaveCamera(current_camera.first, "", model_name, vendor_name);
    }
  }
  return DB_OK;
}

DB_STATUS Database::StartNewTake(const double fps, int &take_id) {

  // Insert new take.
  string sql_string = "INSERT INTO Takes(fps, start_date) VALUES(?, ?)";

  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement in StartNewTake()");

    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_double(prepared_statement, 1, fps) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in StartNewTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  time_t raw_time;
  struct tm *current_time;
  time ( &raw_time );
  current_time = localtime ( &raw_time );

  const int kTimeStringLength = 20;
  char buffer [kTimeStringLength];

  // SQLite expected date string format is "YYYY-MM-DD HH:MM:SS" (there are others too)
  strftime(buffer, kTimeStringLength, "%Y-%m-%d %H:%M:%S", current_time);

  rc = sqlite3_bind_text(prepared_statement, 2, buffer, -1, SQLITE_TRANSIENT);
  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in StartNewTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);
  if (rc != SQLITE_DONE) {
    Logger::Log(DB_CRIT, "Database failed stepping in StartNewTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  rc = sqlite3_reset(prepared_statement);
  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed resetting statement in StartNewTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;        
  }

  // Get take id.
  sql_string = "SELECT last_insert_rowid()";

  rc = sqlite3_prepare_v2(database, 
                          sql_string.c_str(), 
                          -1, 
                          &prepared_statement, 
                          nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement in StartNewTake()");

    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);
  if (rc != SQLITE_ROW) {
    Logger::Log(DB_CRIT, "Database failed while stepping in StartNewTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  int temp_take_id = sqlite3_column_int(prepared_statement, 0);

  sqlite3_finalize(prepared_statement);

  take_id = temp_take_id;

  Logger::Log(DB_INFO, take_id, "Take started.");

  return DB_OK;
}

DB_STATUS Database::EndTake(const int take_id) {

  string sql_string = "UPDATE Takes SET end_date=? WHERE take_id=?";

  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement in EndTake()");

    return DB_SQL_ERROR;
  }

  time_t raw_time;
  struct tm *current_time;
  time ( &raw_time );
  current_time = localtime ( &raw_time );

  const int kTimeStringLength = 20;
  char buffer [kTimeStringLength];

  // SQLite expected date string format is "YYYY-MM-DD HH:MM:SS" (there are others too)
  strftime(buffer, kTimeStringLength, "%Y-%m-%d %H:%M:%S", current_time);

  rc = sqlite3_bind_text(prepared_statement, 1, buffer, -1, SQLITE_TRANSIENT);
  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in EndTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  rc = sqlite3_bind_int(prepared_statement, 2, take_id);
  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in EndTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);
  if (rc != SQLITE_DONE) {
    Logger::Log(DB_CRIT, "Database failed stepping in EndTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  sqlite3_finalize(prepared_statement);

  Logger::Log(DB_INFO, "Take ended.");

  return DB_OK;
}

DB_STATUS Database::GetLatestTakeId(int &take_id) {

  string sql_string = "SELECT MAX(take_id) FROM Takes";

  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement in GetLatestTakeId()");

    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);
  if (rc != SQLITE_ROW) {
    Logger::Log(DB_CRIT, "Database failed stepping in GetLatestTakeId()");

    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  int temp_take_id = sqlite3_column_int(prepared_statement, 0);

  sqlite3_finalize(prepared_statement);

  take_id = temp_take_id;

  return DB_OK;
}

void Database::ClearDatabase() {

  string sql_string = "DELETE FROM Cameras";

  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    return;
  }

  sqlite3_step(prepared_statement);
  sqlite3_finalize(prepared_statement);

  sql_string = "DELETE FROM Takes";

  rc = sqlite3_prepare_v2(database, 
                          sql_string.c_str(), 
                          -1, 
                          &prepared_statement, 
                          nullptr);

  if (rc != SQLITE_OK) {
    return;
  }

  sqlite3_step(prepared_statement);
  sqlite3_finalize(prepared_statement);

  sql_string = "DELETE FROM Images";

  rc = sqlite3_prepare_v2(database, 
                          sql_string.c_str(), 
                          -1, 
                          &prepared_statement, 
                          nullptr);

  if (rc != SQLITE_OK) {
    return;
  }

  sqlite3_step(prepared_statement);
  sqlite3_finalize(prepared_statement);

  Logger::Log(DB_INFO, "Database cleared.");

}

DB_STATUS Database::GetTakes(vector<TakeStruct> &takes) {

  string sql_string = "SELECT * FROM Takes";

  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement in GetTakes()");
    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);
  while (rc != SQLITE_DONE) {

    if (rc != SQLITE_ROW) {
      sqlite3_finalize(prepared_statement);
      Logger::Log(DB_CRIT, "Database failed stepping in GetTakes()");
      return DB_SQL_ERROR; 
    }

    TakeStruct current_take;

    current_take.take_id = sqlite3_column_int(prepared_statement, 0);
    current_take.fps = sqlite3_column_double(prepared_statement, 1);
    const unsigned char *start_date = sqlite3_column_text(prepared_statement, 2);
    if (start_date != nullptr) {
      current_take.start_date = string(reinterpret_cast<const char*>(start_date));
    } else {
      current_take.start_date = "-";
    }

    const unsigned char *end_date = sqlite3_column_text(prepared_statement, 3);
    if (end_date != nullptr) {
      current_take.end_date = string(reinterpret_cast<const char*>(end_date));
    } else {
      current_take.end_date = "-";
    }

    takes.push_back(current_take);

    rc = sqlite3_step(prepared_statement);
  }

  sqlite3_finalize(prepared_statement);

  sql_string = "SELECT COUNT(DISTINCT camera_serial_number) FROM Images WHERE take_id=?";
  rc = sqlite3_prepare_v2(database, 
                          sql_string.c_str(), 
                          -1, 
                          &prepared_statement, 
                          nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement 2 in GetTakes()");
    return DB_SQL_ERROR;
  }

  for (auto &current_take : takes) {
    sqlite3_reset(prepared_statement);

    rc = sqlite3_bind_int(prepared_statement, 1, current_take.take_id);
    if (rc != SQLITE_OK) {
      sqlite3_finalize(prepared_statement);
      Logger::Log(DB_CRIT, "Database failed binding variable in GetTakes()");

      return DB_SQL_ERROR;
    }

    sqlite3_step(prepared_statement);

    current_take.num_cameras = sqlite3_column_int(prepared_statement, 0);

  }

  sqlite3_finalize(prepared_statement);

  return DB_OK;
} 

DB_STATUS Database::GetCameraListFromTake(const int take_id, 
                                          vector<CameraStruct> &cameras) {

  string sql_string = "SELECT DISTINCT camera_serial_number FROM Images WHERE take_id=?";

  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement in GetCameraListFromTake()");

    return DB_OK;
  }

  if (sqlite3_bind_int(prepared_statement, 1, take_id) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in GetCameraListFromTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);
  while (rc != SQLITE_DONE) {

    if (rc != SQLITE_ROW) {
      Logger::Log(DB_CRIT, "Database failed stepping in GetCameraListFromTake()");
      sqlite3_finalize(prepared_statement);

      return DB_SQL_ERROR;
    }

    int temp_serial_number = sqlite3_column_int(prepared_statement, 0);
    if (temp_serial_number > 0) {
    CameraStruct current_camera;
    current_camera.serial_number = temp_serial_number;
    cameras.push_back(current_camera);
    }

    rc = sqlite3_step(prepared_statement);

  }

  sqlite3_finalize(prepared_statement);

  sql_string = "SELECT name FROM Cameras WHERE serial_number=?";

  rc = sqlite3_prepare_v2(database, 
                          sql_string.c_str(), 
                          -1, 
                          &prepared_statement, 
                          nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement 2 in GetCameraListFromTake()");

    return DB_OK;
  }

  for (auto &current_camera : cameras) {
    sqlite3_reset(prepared_statement);

    rc = sqlite3_bind_int(prepared_statement, 1, current_camera.serial_number);

    if (rc != SQLITE_OK) {
      Logger::Log(DB_CRIT, "Database failed binding variable in GetCameraListFromTake()");
      sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
    }

    sqlite3_step(prepared_statement);

    const unsigned char *temp_name = sqlite3_column_text(prepared_statement, 0);
    current_camera.name = string(reinterpret_cast<const char*>(temp_name));

  }

  sqlite3_finalize(prepared_statement);

  return DB_OK;
}

DB_STATUS Database::DeleteTake(const int take_id) {

  string sql_string = "DELETE FROM Takes WHERE take_id=?";

  sqlite3_stmt *prepared_statement;

  int rc = sqlite3_prepare_v2(database, 
                              sql_string.c_str(), 
                              -1, 
                              &prepared_statement, 
                              nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement in DeleteTake()");
    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_int(prepared_statement, 1, take_id) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in DeleteTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);

  if (rc == SQLITE_CONSTRAINT) {
    Logger::Log(DB_CRIT, "Database constraint error in DeleteTake()");
    sqlite3_finalize(prepared_statement);

    return DB_CONSTRAINT_ERROR;
  } else if (rc != SQLITE_DONE) {
    Logger::Log(DB_CRIT, "Database failed stepping in DeleteTake()");

    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  sqlite3_reset(prepared_statement);

  sql_string = "DELETE FROM Images WHERE take_id=?";

  rc = sqlite3_prepare_v2(database, 
                          sql_string.c_str(), 
                          -1, 
                          &prepared_statement, 
                          nullptr);

  if (rc != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed preparing statement 2 in DeleteTake()");
    return DB_SQL_ERROR;
  }

  if (sqlite3_bind_int(prepared_statement, 1, take_id) != SQLITE_OK) {
    Logger::Log(DB_CRIT, "Database failed binding variable in DeleteTake()");
    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }

  rc = sqlite3_step(prepared_statement);

  if (rc == SQLITE_CONSTRAINT) {
    Logger::Log(DB_CRIT, "Database constraint error in DeleteTake()");
    sqlite3_finalize(prepared_statement);

    return DB_CONSTRAINT_ERROR;
  } else if (rc != SQLITE_DONE) {
    Logger::Log(DB_CRIT, "Database failed stepping in DeleteTake()");

    sqlite3_finalize(prepared_statement);

    return DB_SQL_ERROR;
  }


  sqlite3_finalize(prepared_statement);

  return DB_OK;
}
