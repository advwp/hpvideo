#ifndef CAMERA_CON_LOCAL_H
#define CAMERA_CON_LOCAL_H

#include "camera_con.h"
#include "camera.h"

class Camera;
/* \brief This class is used for cameras connected locally. 
 * 
 * Cameras connected via USB etc. uses this class. 
 * Since the camera is running locally, most of the functions 
 * are straight redirects to the camera object. 
 */
class CameraConLocal : public CameraCon {

public:

  /* \brief Constructor
   * 
   * The constructor attempts to create a camera 
   * object. Remember to check IsConnected() to see if
   * the connection succeded. 
   * @param guid PGRGuid of the camera. 
   */
  CameraConLocal(FlyCapture2::PGRGuid guid);

  virtual CAM_INTERFACE_TYPE GetType() { return LOCAL; }

  virtual bool GetNewImage(std::unique_ptr<FlyCapture2::Image> &image);
  virtual bool ReadBuffer(std::unique_ptr<FlyCapture2::Image> &image);

  virtual bool StartLiveView();
  virtual bool StartCapture();
  virtual bool StopCapture();

  virtual bool ReadCameraInfo(std::unique_ptr<FlyCapture2::CameraInfo> &camera_info);

  virtual bool SetVideoModeAndFrameRate(const FlyCapture2::VideoMode video_mode, 
                                        const FlyCapture2::FrameRate frame_rate);

  virtual bool SetFC2Config(const std::unique_ptr<FlyCapture2::FC2Config> &config);

  virtual bool GetFC2Config(std::unique_ptr<FlyCapture2::FC2Config> &config);

  virtual bool GetTrigger(bool &trigger);
  virtual bool SetTrigger(const bool trigger_status);
  virtual bool FireSoftwareTrigger();

  virtual int GetBufferSize() { return camera_->GetBufferSize(); }

  virtual unsigned int GetSerialNumber();

  virtual std::string GetModel();

  virtual bool IsConnected() { return camera_->IsConnected(); }

  virtual bool ReadProperty(std::unique_ptr<FlyCapture2::Property> &property, 
                            FlyCapture2::PropertyType type);

  virtual bool ReadPropertyInfo(std::unique_ptr<FlyCapture2::PropertyInfo> &property_info, 
                                FlyCapture2::PropertyType type);

  virtual bool SetProperty(const std::unique_ptr<FlyCapture2::Property> &property);

  virtual bool CheckVideoModeAndFrameRate(FlyCapture2::VideoMode video_mode, 
                                          FlyCapture2::FrameRate frame_rate);

private: 
  std::unique_ptr<Camera> camera_; //!< Camera object. 

}; // CameraConLocal

#endif // CAMERA_CON_LOCAL_H
