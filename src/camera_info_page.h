#ifndef CAMERA_INFO_PAGE_H
#define CAMERA_INFO_PAGE_H

#include "ui_camera_info_page.h"

#include "FlyCapture2.h"

#include <memory>

class Controller;

/*! \brief Class defining the camera information page. 
*/
class CameraInfoPage : public QWidget, private Ui::camera_info_page {

  Q_OBJECT

public:

  /*! \brief Constructor
   * 
   * @param controller
   * @param camera_vector selected cameras. 
   * @param parent parent widget
   */
  CameraInfoPage(std::shared_ptr<Controller> controller, 
                 std::vector<unsigned int> &camera_vector, 
                 QWidget *parent = 0);

  /*! \brief Initial setup
  */
  void Init();

public slots:

private: 
        
  std::shared_ptr<Controller> controller_; //!< Controller 
  std::vector<unsigned int> camera_vector_; //!< Selected cameras
};

#endif
