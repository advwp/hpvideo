#include "camera_con_local.h"

#include <iostream>
#include <string>

CameraConLocal::CameraConLocal(FlyCapture2::PGRGuid guid) {
  std::unique_ptr<Camera> temp_camera(new Camera(guid));
  camera_ = std::move(temp_camera);
}

bool CameraConLocal::GetNewImage(std::unique_ptr<FlyCapture2::Image> &image) {
  return camera_->GetNewImage(image);
}

bool CameraConLocal::ReadBuffer(std::unique_ptr<FlyCapture2::Image> &image) {
  return camera_->ReadBuffer(image);
}

bool CameraConLocal::StartLiveView() {
  return camera_->StartLiveView();
}

bool CameraConLocal::StartCapture() {
  return camera_->StartCapture();
}

bool CameraConLocal::StopCapture() {
  return camera_->StopCapture();
}

bool CameraConLocal::SetVideoModeAndFrameRate(const FlyCapture2::VideoMode video_mode, 
                              const FlyCapture2::FrameRate frame_rate) {
  return camera_->SetVideoModeAndFrameRate(video_mode, frame_rate);
}

bool CameraConLocal::ReadCameraInfo(std::unique_ptr<FlyCapture2::CameraInfo> &camera_info) {
  return camera_->ReadCameraInfo(camera_info);
}

bool CameraConLocal::GetTrigger(bool &trigger) {
  return camera_->GetTrigger(trigger);
}

bool CameraConLocal::SetTrigger(const bool trigger_status) {
  return camera_->SetTrigger(trigger_status);
}

bool CameraConLocal::FireSoftwareTrigger() {
  return camera_->FireSoftwareTrigger();
}

bool CameraConLocal::SetFC2Config(const std::unique_ptr<FlyCapture2::FC2Config> &config) {
  return camera_->SetFC2Config(config);
}

bool CameraConLocal::GetFC2Config(std::unique_ptr<FlyCapture2::FC2Config> &config) {
  return camera_->GetFC2Config(config);
}

bool CameraConLocal::ReadProperty(std::unique_ptr<FlyCapture2::Property> &property, 
                                  FlyCapture2::PropertyType type) {
  return camera_->ReadProperty(property, type);
}

bool CameraConLocal::ReadPropertyInfo(std::unique_ptr<FlyCapture2::PropertyInfo> &property_info, 
                                      FlyCapture2::PropertyType type) {
  return camera_->ReadPropertyInfo(property_info, type);
}

bool CameraConLocal::SetProperty(const std::unique_ptr<FlyCapture2::Property> &property) {
  return camera_->SetProperty(property);
}

unsigned int CameraConLocal::GetSerialNumber() {

  std::unique_ptr<FlyCapture2::CameraInfo> temp_camera_info;

  if (!ReadCameraInfo(temp_camera_info)) {
    return -1;
  }

  return temp_camera_info->serialNumber;
}

std::string CameraConLocal::GetModel() {

  std::unique_ptr<FlyCapture2::CameraInfo> temp_camera_info;

  if (!ReadCameraInfo(temp_camera_info)) {
    return "";
  }

  std::string model_name(temp_camera_info->modelName);

  return model_name;
}

bool CameraConLocal::CheckVideoModeAndFrameRate(FlyCapture2::VideoMode video_mode, 
                                                FlyCapture2::FrameRate frame_rate) {

  return camera_->CheckVideoModeAndFrameRate(video_mode, frame_rate);
}

