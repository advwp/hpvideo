#ifndef CAMERA_TRIGGER_PAGE_H
#define CAMERA_TRIGGER_PAGE_H

#include "ui_camera_trigger_page.h"

#include "FlyCapture2.h"

#include <memory>

class Controller;

/*! \brief Class controlling trigger options page. 
*/
class CameraTriggerPage : public QWidget, private Ui::camera_trigger_page {

  Q_OBJECT

public:

  /*! \brief Constructor
   * 
   * @param controller
   * @param camera_vector selected cameras. 
   * @param parent parent widget
   */
  CameraTriggerPage(std::shared_ptr<Controller> controller, std::vector<unsigned int> &camera_vector, QWidget *parent = 0);

public slots:
  
  void BtnEnableTriggerClicked();
  void BtnFireSoftwareTriggerClicked();

private: 
  std::shared_ptr<Controller> controller_; //!< Controller
  std::vector<unsigned int> camera_vector_; //!< Selected cameras

  void ConnectSignals();
  void Init();

};

#endif
