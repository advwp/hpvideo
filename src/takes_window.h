#ifndef TAKES_WINDOW_H
#define TAKES_WINDOW_H

#include "ui_takes_window.h"
#include <memory>

class Controller;

/*! \brief Class controlling the takes window
*/
class TakesWindow : public QWidget, private Ui::win_takes {

  Q_OBJECT

public:

  /*! \brief Constructor
   * 
   * @param controller
   * @param parent parent widget
   */
  TakesWindow(std::shared_ptr<Controller> controller, 
              QWidget *parent = 0);

public slots:

  /*! \brief Called when save take button is clicked. 
  */
  void BtnSaveTakeClicked();

  /*! \brief Called when delete take button is clicked. 
  */
  void BtnDeleteTakeClicked();

private:

  /*! \brief Initial setup. 
  */
  void Init();

  /*! \brief Add a list item. 
   * 
   * Used by UpdateTakesList() to show takes found. 
   * @param take_id
   * @param start_date
   * @param end_date
   * @param num_cameras
   * @param fps
   */
  void AddTakeToList(const QString &take_id, 
                     const QString &start_date, 
                     const QString &end_date, 
                     const QString &num_cameras, 
                     const QString &fps);

  /*! \brief Connect signals
  */
  void ConnectSignals();

  /*! \brief Updates the list of takes from the database. 
   * 
   * Uses AddTakeToList() to display the takes. 
   */
  void UpdateTakesList();

  /*! \brief Get the current selection. 
   * 
   * @param *OUT* take_id
   * @param *OUT* fps
   */
  void GetSelection(int &take_id, float &fps);

  std::shared_ptr<Controller> controller_; //!< Controller

};

#endif
