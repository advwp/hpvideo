#ifndef STATIC_INFO_H
#define STATIC_INFO_H

#include <string>

enum STATUS {

  CAM_OK, 
  CAM_FAIL, 
  CAM_CONNECT_FAIL, 
  CAM_GETINFO_FAIL, 
  CAM_GET_PROPERTY_FAIL, 
  CAM_SET_PROPERTY_FAIL, 
  CAM_GET_PROPERTY_INFO_FAIL, 
  CAM_BUFFER_EMPTY, 
  CAM_LIVE_VIEW_START_FAIL, 
  CAM_CAPTURE_START_FAIL, 
  CAM_CAPTURE_STOP_FAIL, 
  CAM_GET_TRIGGER_FAIL, 
  CAM_SET_TRIGGER_FAIL, 
  CAM_IMAGE_RETRIEVAL_FAIL

};

enum DB_STATUS {

  DB_OK,
  DB_ERROR, 
  DB_NOT_FOUND, 
  DB_SQL_ERROR, 
  DB_CONSTRAINT_ERROR

};

enum CAM_INTERFACE_TYPE {

  LOCAL, 
  REMOTE

};

enum LOG_LEVEL {

  CAM_INFO, 
  CAM_CRIT, 
  DB_INFO, 
  DB_CRIT

};

/*! \brief Variables which should be stored. 
 *
 * The top part of the enum must be equal to LOG_LEVEL. 
 */
enum GLOBAL_VARIABLES {

  LOG_CAM_INFO, 
  LOG_CAM_CRIT, 
  LOG_DB_INFO, 
  LOG_DB_CRIT, 
  STORE_IMAGES, 
  USE_SOFTWARE_TRIGGER, 
  NOT_FOUND

};

struct TakeStruct {

  int take_id;
  std::string start_date;
  std::string end_date;
  int num_cameras;
  double fps;

};

struct CameraStruct {

  unsigned int serial_number;
  std::string name;

};

const std::string DB_NAME = "database.db";
const std::string kLogFileName = "log.txt";
static const std::string kSettingsFileName = "settings.cfg";
static const std::string kTmpSettingsFileName = "settings.cfg.tmp";

#endif
