#ifndef RECORD_WINDOW_H
#define RECORD_WINDOW_H

#include "ui_record_window.h"

#include <vector>
#include <memory>

class Controller;

/*! \brief Class controlling the record window
*/
class RecordWindow : public QWidget, private Ui::win_record {

  Q_OBJECT

public:

  /*! \brief Constructor
   * 
   * @param controller
   * @param camera_vector selected cameras. 
   * @param parent parent widget
   */
  RecordWindow(std::shared_ptr<Controller> controller, 
               std::vector<unsigned int> &camera_vector, 
               QWidget *parent = 0);

  /*! \brief Destructor
   *
   * Makes sure captures are stopped before exiting. 
   */
  ~RecordWindow();

public slots:
  
  /*! \brief Called when the start record button is clicked. 
   */
  void BtnStartRecordClicked();

  /*! \brief Called when the stop record button is clicked. 
   */
  void BtnStopRecordClicked();

  /*! \brief Called when the save images radio button is clicked. 
   */
  void BtnSaveImagesClicked();

  /*! \brief Called when the transfer images radio button is clicked. 
   */
  void BtnTransferImagesClicked();

  /*! \brief Updates the progress bar. 
   *
   * This function is called by the timer and updates
   * the progress bar. 
   */
  void UpdateProgressBar();

  /*! \brief Fires software triggers. 
   *
   * This function is called by trigger_timer. 
   * The speed depends on the fps set in the GUI.
   */
  void FireSoftwareTriggers();


private:
  /*! \brief Initial setup
  */
  void Init();

  /*! \brief Connect signals
  */
  void ConnectSignals();

  /*! \brief Updates selection_
  */
  void SetCameras(std::vector<unsigned int> &cameras);

  std::vector<unsigned int> selection_; //!< Selected cameras in this window. 

  std::vector<unsigned int> camera_vector_; //!< Selected cameras in main win. 

  std::shared_ptr<Controller> controller_; //!< Controller

  QTimer *timer_; //!< Timer for updating progress bar. 
  QTimer *trigger_timer_; //!< Timer for firing software trigger at fps set.
};

#endif
