#include <QtGui>

#include "camera_info_page.h"

#include "controller.h"

CameraInfoPage::CameraInfoPage(std::shared_ptr<Controller> controller, 
                               std::vector<unsigned int> &camera_vector, 
                               QWidget *parent) : QWidget(parent) {

  setupUi(this);

  for (auto serial_number : camera_vector) {
    camera_vector_.push_back(serial_number);
  }

  this->controller_ = controller;

  Init();

}

void CameraInfoPage::Init() {

  std::unique_ptr<FlyCapture2::CameraInfo> camera_info;

  if (!controller_->ReadCameraInfo(camera_vector_.at(0), camera_info)) {
    return;
  }

  QString q_str_serial_number;
  q_str_serial_number.setNum(camera_info->serialNumber, 10);
  lbl_serial_number_2->setText(q_str_serial_number);

  QString q_str_model_name(camera_info->modelName);
  lbl_model_2->setText(q_str_model_name);

  QString q_str_vendor_name(camera_info->vendorName);
  lbl_vendor_2->setText(q_str_vendor_name);

  QString q_str_sensor_info(camera_info->sensorInfo);
  lbl_sensor_2->setText(q_str_sensor_info);

  QString q_str_sensor_resolution(camera_info->sensorResolution);
  lbl_resolution_2->setText(q_str_sensor_resolution);

  QString q_str_interface_type;
  switch (camera_info->interfaceType) {
    case FlyCapture2::INTERFACE_IEEE1394:
      q_str_interface_type = "IEEE-1394";
      break;
    case FlyCapture2::INTERFACE_USB2: 
      q_str_interface_type = "USB2.0";
      break;
    case FlyCapture2::INTERFACE_GIGE: 
      q_str_interface_type = "GigE";
      break;
    case FlyCapture2::INTERFACE_UNKNOWN: 
    default: 
      q_str_interface_type = "Unknown";
      break;
  }
  lbl_interface_2->setText(q_str_interface_type);

  QString q_str_bus_speed;
  switch (camera_info->maximumBusSpeed) {
  case FlyCapture2::BUSSPEED_S100:
    q_str_bus_speed =          "100Mbits/sec.";
    break;
  case FlyCapture2::BUSSPEED_S200:
    q_str_bus_speed =          "200Mbits/sec.";
    break;
  case FlyCapture2::BUSSPEED_S400:
    q_str_bus_speed =         "400Mbits/sec.";
    break;
  case FlyCapture2::BUSSPEED_S480:
    q_str_bus_speed =         "480Mbits/sec.";
    break;
  case FlyCapture2::BUSSPEED_S800:
    q_str_bus_speed =         "800Mbits/sec.";
    break;
  case FlyCapture2::BUSSPEED_S1600:
    q_str_bus_speed =         "1600Mbits/sec.";
    break;
  case FlyCapture2::BUSSPEED_S3200:
    q_str_bus_speed =         "3200Mbits/sec.";
    break;
  case FlyCapture2::BUSSPEED_10BASE_T:
    q_str_bus_speed =         "10Base-T.";
    break;
  case FlyCapture2::BUSSPEED_100BASE_T:
    q_str_bus_speed =         "100Base-T.";
    break;
  case FlyCapture2::BUSSPEED_1000BASE_T:
    q_str_bus_speed =         "1000Base-T (Gigabit Ethernet).";
    break;
  case FlyCapture2::BUSSPEED_10000BASE_T:
    q_str_bus_speed =         "10000Base-T.";
    break;
  case FlyCapture2::BUSSPEED_S_FASTEST:
    q_str_bus_speed =         "The fastest speed available.";
    break;
  case FlyCapture2::BUSSPEED_ANY:
    q_str_bus_speed =         "Any speed that is available.";
    break;
  default:
  case FlyCapture2::BUSSPEED_SPEED_UNKNOWN:
    q_str_bus_speed =         "Unknown bus speed.";
    break;
  }
  lbl_bus_speed_2->setText(q_str_bus_speed);

  QString q_str_iidc_version;
  q_str_iidc_version.setNum(camera_info->iidcVer, 10);
  lbl_iidc_version_2->setText(q_str_iidc_version);

  QString q_str_firmware_version(camera_info->firmwareVersion);
  lbl_firmware_version_2->setText(q_str_firmware_version);

  QString q_str_firmware_build_time(camera_info->firmwareBuildTime);
  lbl_firmware_build_time_2->setText(q_str_firmware_build_time);

  QString q_str_driver_name(camera_info->driverName);
  lbl_driver_2->setText(q_str_driver_name);

}
