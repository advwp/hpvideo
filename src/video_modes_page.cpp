#include <QtGui>

#include "video_modes_page.h"

#include "controller.h"

VideoModesPage::VideoModesPage(std::shared_ptr<Controller> controller, 
                         std::vector<unsigned int> &camera_vector, 
                         QWidget *parent) : QWidget(parent) {
  setupUi(this);

  for (auto serial_number : camera_vector) {
    camera_vector_.push_back(serial_number);
  }

  this->controller_ = controller;

  Init();
}

void VideoModesPage::Init() {

  video_mode_signal_mapper_ = new QSignalMapper(this);
  frame_rate_signal_mapper_ = new QSignalMapper(this);

  SetupMaps();
  SetupButtonWidgets();
  ConnectSignals();
}

void VideoModesPage::ConnectSignals() {

  connect(video_mode_signal_mapper_, SIGNAL( mapped(int) ), this, SLOT( VideoModeButtonClicked(int) ));
  connect(frame_rate_signal_mapper_, SIGNAL( mapped(int) ), this, SLOT( FrameRateButtonClicked(int) ));

  for (auto &it : video_mode_button_map_) {
    video_mode_signal_mapper_->setMapping(it.second, static_cast<int>(it.first));
    connect(it.second, SIGNAL( released() ), video_mode_signal_mapper_, SLOT( map() ));
  }

  for (auto &it : frame_rate_button_map_) {
    frame_rate_signal_mapper_->setMapping(it.second, static_cast<int>(it.first));
    connect(it.second, SIGNAL( released() ), frame_rate_signal_mapper_, SLOT( map() ));
  }

}

void VideoModesPage::SetupMaps() {

  video_mode_button_map_[FlyCapture2::VIDEOMODE_160x120YUV444] = btn_160x120YUV444;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_320x240YUV422] = btn_320x240YUV422;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_640x480YUV411] = btn_640x480YUV411;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_640x480YUV422] = btn_640x480YUV422;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_640x480RGB] = btn_640x480RGB;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_640x480Y8] = btn_640x480Y8;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_640x480Y16] = btn_640x480Y16;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_800x600YUV422] = btn_800x600YUV422;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_800x600RGB] = btn_800x600RGB;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_800x600Y8] = btn_800x600Y8;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_800x600Y16] = btn_800x600Y16;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1024x768YUV422] = btn_1024x768YUV422;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1024x768RGB] = btn_1024x768RGB;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1024x768Y8] = btn_1024x768Y8;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1024x768Y16] = btn_1024x768Y16;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1280x960YUV422] = btn_1280x960YUV422;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1280x960RGB] = btn_1280x960RGB;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1280x960Y8] = btn_1280x960Y8;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1280x960Y16] = btn_1280x960Y16;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1600x1200YUV422] = btn_1600x1200YUV422;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1600x1200RGB] = btn_1600x1200RGB;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1600x1200Y8] = btn_1600x1200Y8;
  video_mode_button_map_[FlyCapture2::VIDEOMODE_1600x1200Y16] = btn_1600x1200Y16;

  frame_rate_button_map_[FlyCapture2::FRAMERATE_1_875] = btn_1_875;
  frame_rate_button_map_[FlyCapture2::FRAMERATE_3_75] = btn_3_75;
  frame_rate_button_map_[FlyCapture2::FRAMERATE_7_5] = btn_7_5;
  frame_rate_button_map_[FlyCapture2::FRAMERATE_15] = btn_15;
  frame_rate_button_map_[FlyCapture2::FRAMERATE_30] = btn_30;
  frame_rate_button_map_[FlyCapture2::FRAMERATE_60] = btn_60;
  frame_rate_button_map_[FlyCapture2::FRAMERATE_120] = btn_120;
  frame_rate_button_map_[FlyCapture2::FRAMERATE_240] = btn_240;
}

void VideoModesPage::SetupButtonWidgets() {

  bool frameRateSupported = false;
  for (auto current_serial_number : camera_vector_) {
          
    for (auto current_video_mode : video_mode_button_map_) {

      frameRateSupported = false;
      for (auto current_frame_rate : frame_rate_button_map_) {

        if (controller_->CheckVideoModeAndFrameRate(current_serial_number, 
                                             current_video_mode.first, 
                                             current_frame_rate.first)) {

          frameRateSupported = true;
          video_mode_button_map_[current_video_mode.first]->show();
          break;
        }

      }

      if (!frameRateSupported) {
        video_mode_button_map_[current_video_mode.first]->hide();
      }

    }
  }
}

void VideoModesPage::VideoModeButtonClicked(int mode) {
  FlyCapture2::VideoMode video_mode = static_cast<FlyCapture2::VideoMode>(mode);
  FlyCapture2::FrameRate frame_rate;

  for (auto &it : frame_rate_button_map_) {
    if (controller_->CheckVideoModeAndFrameRate(camera_vector_.at(0), video_mode, it.first)) {
      frame_rate = it.first;
      break;
    }
  }

  for (auto &it : camera_vector_) {
    controller_->SetVideoModeAndFrameRate(it, video_mode, frame_rate);
  }

  UpdateFrameRates(video_mode);
}

void VideoModesPage::FrameRateButtonClicked(int rate) {
  FlyCapture2::FrameRate frame_rate = static_cast<FlyCapture2::FrameRate>(rate);
  FlyCapture2::VideoMode video_mode;

  for (auto &it : video_mode_button_map_) {
    if (it.second->isChecked()) {
      video_mode = it.first;
      break;
    }
  }

  for (auto &it : camera_vector_) {
    controller_->SetVideoModeAndFrameRate(it, video_mode, frame_rate);
  }
}

void VideoModesPage::UpdateFrameRates(FlyCapture2::VideoMode video_mode) {
  for (auto &it : frame_rate_button_map_) {
    if (controller_->CheckVideoModeAndFrameRate(camera_vector_.at(0), 
                                                video_mode, 
                                                it.first)) {
      it.second->show();
    } else {
      it.second->hide();
    }
  }

}
