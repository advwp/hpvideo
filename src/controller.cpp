#include "controller.h"

#include "logger.h"
#include "database.h"
#include "camera_con_local.h"
#include "file_writer.h"
#include "static_info.h"
#include "global_settings.h"

using std::thread;
using std::vector;
using std::map;
using std::shared_ptr;
using std::unique_ptr;
using std::make_shared;
using std::string;
using std::move;
using std::mutex;

Controller::Controller() {

  capturing_ = make_shared<bool>(false);
  capturing_mutex_ = make_shared<std::mutex>();
  images_remaining_ = make_shared<int>(0);
  max_images_remaining_ = make_shared<int>(0);

}

Controller::~Controller() {

}

bool Controller::StartLiveView(const unsigned int serial_number) {

  auto camera = all_cameras_.find(serial_number);

  if (camera == all_cameras_.end()) {
    return false;
  }

  if (!camera->second->StartLiveView()) {
    return false;
  }

  *capturing_ = true;

  return true;
}

bool Controller::StopLiveView(const unsigned int serial_number) {

  auto camera = all_cameras_.find(serial_number);

  if (camera == all_cameras_.end()) {
    return false;
  }
  *capturing_ = false;
  camera->second->StopCapture();
  return true;
}

bool Controller::StartCapture(const vector<unsigned int> &cameras, 
                              const double frame_rate) {
  capturing_mutex_->lock();
  bool cap = *capturing_;
  capturing_mutex_->unlock();

  if (cap) {
    this->StopCapture();
  }

  auto capture_cameras = make_shared<vector<shared_ptr<CameraConLocal>>>();

  for (auto &current_serial_number : cameras) {

    auto current_camera = all_cameras_.find(current_serial_number);

    if (current_camera == all_cameras_.end()) {
      break;
    }
    if (!current_camera->second->StartCapture()) {
      this->StopCapture();

      return false;
    }

    // Capture successfully started. Push camera on capture vector. 
    capture_cameras->push_back(current_camera->second);
  }


  capturing_mutex_->lock();
  *capturing_ = true;
  capturing_mutex_->unlock();

  if (GlobalSettings::GetVariable(STORE_IMAGES) == 1) {
    Logger::Log(CAM_INFO, "Started saving images.");
    record_ = true;
    threads_.push_back(thread(RecordImages, capture_cameras, capturing_, capturing_mutex_, frame_rate));
  } else {
    record_ = false;
    Logger::Log(CAM_INFO, "Did not start to save images.");
  }

  return true;
}

void Controller::StopCapture() {

  capturing_mutex_->lock();
  *capturing_ = false;
  capturing_mutex_->unlock();

  for (auto &current_thread : threads_) {
    current_thread.join();
  }
  threads_.clear();

  for (auto &current_camera : all_cameras_) {
    current_camera.second->StopCapture();
  }

  // If the cameras was set to record images. Start another thread saving the
  // rest of the image buffers.
  if (record_) {
    auto capture_camera_vector = make_shared<vector<shared_ptr<CameraConLocal>>>();

    for (auto &current_camera : all_cameras_) {

      if (current_camera.second->GetBufferSize() > 0) {
        capture_camera_vector->push_back(current_camera.second);
      }

    }
    threads_.push_back(thread(RecordBuffer, 
                              capture_camera_vector, 
                              images_remaining_, 
                              max_images_remaining_));

    Logger::Log(CAM_INFO, "Connecting threads.");
    for (auto &current_thread : threads_) {
      current_thread.join();
    }
    Logger::Log(CAM_INFO, "Threads gone.");
    threads_.clear();

  }

}

void Controller::UpdateCameras() {

  Database database(kDatabaseName);
  if (!database.connected_status()) {
    return;
  }

  all_cameras_.clear();

  vector<unique_ptr<CameraConLocal>> cameras;
  
  camera_scanner_.ScanLocalCameras(cameras);

  for (auto &current_camera : cameras) {

    unique_ptr<FlyCapture2::CameraInfo> camera_info;

    if (!current_camera->ReadCameraInfo(camera_info)) {
      continue;
    }

    all_cameras_[camera_info->serialNumber] = move(current_camera);

  }

  database.UpdateCameras(all_cameras_);

}

bool Controller::ChangeCameraName(const unsigned int serial_number, const string name) {

  Database database(kDatabaseName);
  if (!database.connected_status()) {
    return false;
  }


  DB_STATUS status = database.SetCameraName(serial_number, name);

  if (status != DB_OK) {
    return false;
  }

  return true;
}

bool Controller::GetCameraName(const unsigned int serial_number, string &name) {

  Database database(kDatabaseName);
  if (!database.connected_status()) {
    return false;
  }
  std::string tmp_name;

  DB_STATUS status = database.GetCameraName(serial_number, tmp_name);

  if (status != DB_OK) {
    return false;
  }

  if (tmp_name.size() < 1) {
    return false;
  }

  name = tmp_name;

  return true;
}

bool Controller::GetImageData(const unsigned int serial_number, 
                              unique_ptr<unsigned char> &image_data, 
                              unsigned int &image_data_size, 
                              unsigned int &image_width, 
                              unsigned int &image_height) {

  if (!(*capturing_)) {
    return false;
  }

  if (all_cameras_.count(serial_number) <= 0) {
    return false;
  }

  std::unique_ptr<FlyCapture2::Image> raw_image;
  if (!all_cameras_.find(serial_number)->second->GetNewImage(raw_image)) {
    return false;
  }

  FlyCapture2::Image converted_image;

  FlyCapture2::Error error;
  error = raw_image->Convert(FlyCapture2::PIXEL_FORMAT_MONO8, &converted_image);

  // Image conversion failed. 
  if (error != FlyCapture2::PGRERROR_OK) {
    return false;
  }

  image_data_size = converted_image.GetDataSize();
  unique_ptr<unsigned char> temp_image_data(new unsigned char[image_data_size]);

  memcpy(temp_image_data.get(), converted_image.GetData(), image_data_size);
  image_data = move(temp_image_data);

  image_width = converted_image.GetCols();
  image_height = converted_image.GetRows();

  return true;
}

bool Controller::SaveCamera(const unsigned int serial_number, 
                            const string name, 
                            const string model_name, 
                            const string vendor_name) {

  Database database(kDatabaseName);
  if (!database.connected_status()) {
    return false;
  }

  DB_STATUS status = database.SaveCamera(serial_number, name, model_name, vendor_name);

  if (status != DB_OK) {
    return false;
  }

  return true;
}

bool Controller::SetVideoModeAndFrameRate(const unsigned int serial_number, 
                                          const FlyCapture2::VideoMode video_mode, 
                                          const FlyCapture2::FrameRate frame_rate) {
  if (all_cameras_.count(serial_number) < 1) {
    return false;
  } 

  return all_cameras_[serial_number]->SetVideoModeAndFrameRate(video_mode, frame_rate);
}

void Controller::WriteImagesToAvi(const unsigned int serial_number, const int take_id, const float frame_rate) {

  Database database(kDatabaseName);
  if (!database.connected_status()) {
    return;
  }

  FileWriter file_writer;
  
  vector<unique_ptr<FlyCapture2::Image>> images;

  if (database.GetImages(serial_number, take_id, images) != DB_OK) {
    return; 
  }

  file_writer.WriteToAvi(serial_number, take_id, frame_rate, images);
}

bool Controller::ReadProperty(unsigned int serial_number, 
                              unique_ptr<FlyCapture2::Property> &property, 
                              const FlyCapture2::PropertyType type) {

  if (all_cameras_.count(serial_number) <= 0) {
    return false;
  }

  unique_ptr<FlyCapture2::Property> temp_property;

  if (!all_cameras_[serial_number]->ReadProperty(temp_property, type)) {
    property = nullptr;
    return false;
  }

  property = move(temp_property);

  return true;

}

bool Controller::SetProperty(const unsigned int serial_number, 
                             const unique_ptr<FlyCapture2::Property> &property) {

  if (all_cameras_.count(serial_number) <= 0) {
    return false;
  }

  if (!all_cameras_[serial_number]->SetProperty(property)) {
    return false;
  }

  return true;
}

bool Controller::ReadPropertyInfo(unsigned int serial_number, 
                                  unique_ptr<FlyCapture2::PropertyInfo> &property_info, 
                                  FlyCapture2::PropertyType type) {

  if (all_cameras_.count(serial_number) <= 0) {
    property_info = nullptr;
    return false;
  }

  unique_ptr<FlyCapture2::PropertyInfo> temp_property_info;

  if (!all_cameras_[serial_number]->ReadPropertyInfo(temp_property_info, type)) {
    property_info = nullptr;
    return false;
  }

  property_info = move(temp_property_info);

  return true;
}

bool Controller::ReadCameraInfo(unsigned int serial_number, 
                                unique_ptr<FlyCapture2::CameraInfo> &camera_info) {

  if (all_cameras_.count(serial_number) <= 0) {
    camera_info = nullptr;
    return false;
  }

  unique_ptr<FlyCapture2::CameraInfo> temp_camera_info;

  if (!all_cameras_[serial_number]->ReadCameraInfo(temp_camera_info)) {
    camera_info = nullptr;
    return false;
  }

  camera_info = move(temp_camera_info);

  return true;
}

bool Controller::SetFC2Config(unsigned int serial_number, 
                              const std::unique_ptr<FlyCapture2::FC2Config> &config) {

  if (all_cameras_.count(serial_number) < 1) {
    return false;
  }
  return all_cameras_[serial_number]->SetFC2Config(config);
}

bool Controller::GetFC2Config(unsigned int serial_number, 
                              std::unique_ptr<FlyCapture2::FC2Config> &config) {

  if (all_cameras_.count(serial_number) < 1) {
    return false;
  }
  return all_cameras_[serial_number]->GetFC2Config(config);
}

bool Controller::GetTriggerOnOff(const unsigned int serial_number, 
                                 bool &trigger) {
  if (all_cameras_.count(serial_number) <= 0) {
    return false;
  }
  return all_cameras_[serial_number]->GetTrigger(trigger);
}

bool Controller::SetTriggerOnOff(const unsigned int serial_number, 
                                const bool trigger) {
  if (all_cameras_.count(serial_number) <= 0) {
    return false;
  }
  return all_cameras_[serial_number]->SetTrigger(trigger);
}

bool Controller::FireSoftwareTrigger(const unsigned int serial_number) {
  if (all_cameras_.count(serial_number) <= 0) {
    return false;
  }
  return all_cameras_[serial_number]->FireSoftwareTrigger();
}

bool Controller::CheckVideoModeAndFrameRate(const unsigned int serial_number, 
                                            const FlyCapture2::VideoMode video_mode, 
                                            const FlyCapture2::FrameRate frame_rate) {

  if (all_cameras_.find(serial_number) == all_cameras_.end()) {
    return false;
  }

  return all_cameras_[serial_number]->CheckVideoModeAndFrameRate(video_mode, frame_rate);
}

void Controller::RecordImages(const shared_ptr<vector<shared_ptr<CameraConLocal>>> cameras,  
                              const shared_ptr<bool> capturing, 
                              const shared_ptr<mutex> capturing_mutex,
                              const double frame_rate) {

  Database database(kDatabaseName);
  if (!database.connected_status()) {
    capturing_mutex->lock();
    *capturing = false;
    capturing_mutex->unlock();
    return;
  }

  int take_id;
  database.StartNewTake(frame_rate, take_id); 

  while (true) {

    capturing_mutex->lock();
    if (!*capturing) {
      break;
    }
    capturing_mutex->unlock();

    for (unsigned int i = 0; i < cameras.get()->size(); i++) {

      unique_ptr<FlyCapture2::Image> image;

      if (cameras->at(i)->ReadBuffer(image)) {
        database.SaveImage(image, cameras->at(i)->GetSerialNumber(), take_id);
      }

    } // for

  } // while

}

void Controller::RecordBuffer(const shared_ptr<vector<shared_ptr<CameraConLocal>>> cameras,  
                              const shared_ptr<int> images_remaining, 
                              const shared_ptr<int> max_images_remaining) {

  Database database(kDatabaseName);
  if (!database.connected_status()) {
    return;
  }

  *images_remaining = 0;

  int take_id;
  database.GetLatestTakeId(take_id);

  int img_remain = 0;

  for (auto &it : *cameras) {
    img_remain += it->GetBufferSize(); 
  }


  *max_images_remaining = *images_remaining;

  while (img_remain > 0) {

    for (unsigned int i = 0; i < cameras->size(); i++) {
      unique_ptr<FlyCapture2::Image> image;

      if (!cameras->at(i)->ReadBuffer(image)) {

      } else {
        database.SaveImage(image, cameras->at(i)->GetSerialNumber(), take_id);
        //*images_remaining = *images_remaining - 1;
        img_remain--;

      } // else

    } // for

  } // while
  Logger::Log(CAM_INFO, "going to database to end take");
  database.EndTake(take_id);
}


