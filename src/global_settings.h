#ifndef GLOBAL_SETTINGS_H
#define GLOBAL_SETTINGS_H

#include "static_info.h"

/*! \brief Class for storing and getting global settings. 
 *
 * This class is used to store and get global variables. 
 * Available variables are shown in the GLOBAL_VARIABLES enum defined 
 * in static_info.h. Note all global variables are ints. 
 */
class GlobalSettings {

public: 

  /*! \brief Set a global variable.
   *
   * @param var variable to be set.
   * @param val new value.
   */
  static void SetVariable(const GLOBAL_VARIABLES var, const int val);

  /*! \brief Get a global variable. 
   *
   * @param var variable to be set. 
   * @return variable setting or -1 if variable name not found. 
   */
  static int GetVariable(const GLOBAL_VARIABLES var);

private:
  static const std::string GLOBAL_VARIABLES_NAMES[];

};

#endif
